# This Python file uses the following encoding: utf-8
"""autogenerated by genpy from box_messages/TransformMeRequest.msg. Do not edit."""
import sys
python3 = True if sys.hexversion > 0x03000000 else False
import genpy
import struct

import genpy
import geometry_msgs.msg
import visualization_msgs.msg
import std_msgs.msg

class TransformMeRequest(genpy.Message):
  _md5sum = "2fb22dade6e7136108436c8be86ca1d6"
  _type = "box_messages/TransformMeRequest"
  _has_header = False #flag to mark the presence of a Header object
  _full_text = """visualization_msgs/Marker in_marker

================================================================================
MSG: visualization_msgs/Marker
# See http://www.ros.org/wiki/rviz/DisplayTypes/Marker and http://www.ros.org/wiki/rviz/Tutorials/Markers%3A%20Basic%20Shapes for more information on using this message with rviz

uint8 ARROW=0
uint8 CUBE=1
uint8 SPHERE=2
uint8 CYLINDER=3
uint8 LINE_STRIP=4
uint8 LINE_LIST=5
uint8 CUBE_LIST=6
uint8 SPHERE_LIST=7
uint8 POINTS=8
uint8 TEXT_VIEW_FACING=9
uint8 MESH_RESOURCE=10
uint8 TRIANGLE_LIST=11

uint8 ADD=0
uint8 MODIFY=0
uint8 DELETE=2
#uint8 DELETEALL=3 # TODO: enable for ROS-J, disabled for now but functionality is still there. Allows one to clear all markers in plugin

Header header                        # header for time/frame information
string ns                            # Namespace to place this object in... used in conjunction with id to create a unique name for the object
int32 id 		                         # object ID useful in conjunction with the namespace for manipulating and deleting the object later
int32 type 		                       # Type of object
int32 action 	                       # 0 add/modify an object, 1 (deprecated), 2 deletes an object, 3 deletes all objects
geometry_msgs/Pose pose                 # Pose of the object
geometry_msgs/Vector3 scale             # Scale of the object 1,1,1 means default (usually 1 meter square)
std_msgs/ColorRGBA color             # Color [0.0-1.0]
duration lifetime                    # How long the object should last before being automatically deleted.  0 means forever
bool frame_locked                    # If this marker should be frame-locked, i.e. retransformed into its frame every timestep

#Only used if the type specified has some use for them (eg. POINTS, LINE_STRIP, ...)
geometry_msgs/Point[] points
#Only used if the type specified has some use for them (eg. POINTS, LINE_STRIP, ...)
#number of colors must either be 0 or equal to the number of points
#NOTE: alpha is not yet used
std_msgs/ColorRGBA[] colors

# NOTE: only used for text markers
string text

# NOTE: only used for MESH_RESOURCE markers
string mesh_resource
bool mesh_use_embedded_materials

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

================================================================================
MSG: geometry_msgs/Pose
# A representation of pose in free space, composed of postion and orientation. 
Point position
Quaternion orientation

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

================================================================================
MSG: geometry_msgs/Quaternion
# This represents an orientation in free space in quaternion form.

float64 x
float64 y
float64 z
float64 w

================================================================================
MSG: geometry_msgs/Vector3
# This represents a vector in free space. 
# It is only meant to represent a direction. Therefore, it does not
# make sense to apply a translation to it (e.g., when applying a 
# generic rigid transformation to a Vector3, tf2 will only apply the
# rotation). If you want your data to be translatable too, use the
# geometry_msgs/Point message instead.

float64 x
float64 y
float64 z
================================================================================
MSG: std_msgs/ColorRGBA
float32 r
float32 g
float32 b
float32 a
"""
  __slots__ = ['in_marker']
  _slot_types = ['visualization_msgs/Marker']

  def __init__(self, *args, **kwds):
    """
    Constructor. Any message fields that are implicitly/explicitly
    set to None will be assigned a default value. The recommend
    use is keyword arguments as this is more robust to future message
    changes.  You cannot mix in-order arguments and keyword arguments.

    The available fields are:
       in_marker

    :param args: complete set of field values, in .msg order
    :param kwds: use keyword arguments corresponding to message field names
    to set specific fields.
    """
    if args or kwds:
      super(TransformMeRequest, self).__init__(*args, **kwds)
      #message fields cannot be None, assign default values for those that are
      if self.in_marker is None:
        self.in_marker = visualization_msgs.msg.Marker()
    else:
      self.in_marker = visualization_msgs.msg.Marker()

  def _get_types(self):
    """
    internal API method
    """
    return self._slot_types

  def serialize(self, buff):
    """
    serialize message into buffer
    :param buff: buffer, ``StringIO``
    """
    try:
      _x = self
      buff.write(_struct_3I.pack(_x.in_marker.header.seq, _x.in_marker.header.stamp.secs, _x.in_marker.header.stamp.nsecs))
      _x = self.in_marker.header.frame_id
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      if python3:
        buff.write(struct.pack('<I%sB'%length, length, *_x))
      else:
        buff.write(struct.pack('<I%ss'%length, length, _x))
      _x = self.in_marker.ns
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      if python3:
        buff.write(struct.pack('<I%sB'%length, length, *_x))
      else:
        buff.write(struct.pack('<I%ss'%length, length, _x))
      _x = self
      buff.write(_struct_3i10d4f2iB.pack(_x.in_marker.id, _x.in_marker.type, _x.in_marker.action, _x.in_marker.pose.position.x, _x.in_marker.pose.position.y, _x.in_marker.pose.position.z, _x.in_marker.pose.orientation.x, _x.in_marker.pose.orientation.y, _x.in_marker.pose.orientation.z, _x.in_marker.pose.orientation.w, _x.in_marker.scale.x, _x.in_marker.scale.y, _x.in_marker.scale.z, _x.in_marker.color.r, _x.in_marker.color.g, _x.in_marker.color.b, _x.in_marker.color.a, _x.in_marker.lifetime.secs, _x.in_marker.lifetime.nsecs, _x.in_marker.frame_locked))
      length = len(self.in_marker.points)
      buff.write(_struct_I.pack(length))
      for val1 in self.in_marker.points:
        _x = val1
        buff.write(_struct_3d.pack(_x.x, _x.y, _x.z))
      length = len(self.in_marker.colors)
      buff.write(_struct_I.pack(length))
      for val1 in self.in_marker.colors:
        _x = val1
        buff.write(_struct_4f.pack(_x.r, _x.g, _x.b, _x.a))
      _x = self.in_marker.text
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      if python3:
        buff.write(struct.pack('<I%sB'%length, length, *_x))
      else:
        buff.write(struct.pack('<I%ss'%length, length, _x))
      _x = self.in_marker.mesh_resource
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      if python3:
        buff.write(struct.pack('<I%sB'%length, length, *_x))
      else:
        buff.write(struct.pack('<I%ss'%length, length, _x))
      buff.write(_struct_B.pack(self.in_marker.mesh_use_embedded_materials))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(locals().get('_x', self)))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(locals().get('_x', self)))))

  def deserialize(self, str):
    """
    unpack serialized message in str into this message instance
    :param str: byte array of serialized message, ``str``
    """
    try:
      if self.in_marker is None:
        self.in_marker = visualization_msgs.msg.Marker()
      end = 0
      _x = self
      start = end
      end += 12
      (_x.in_marker.header.seq, _x.in_marker.header.stamp.secs, _x.in_marker.header.stamp.nsecs,) = _struct_3I.unpack(str[start:end])
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.in_marker.header.frame_id = str[start:end].decode('utf-8')
      else:
        self.in_marker.header.frame_id = str[start:end]
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.in_marker.ns = str[start:end].decode('utf-8')
      else:
        self.in_marker.ns = str[start:end]
      _x = self
      start = end
      end += 117
      (_x.in_marker.id, _x.in_marker.type, _x.in_marker.action, _x.in_marker.pose.position.x, _x.in_marker.pose.position.y, _x.in_marker.pose.position.z, _x.in_marker.pose.orientation.x, _x.in_marker.pose.orientation.y, _x.in_marker.pose.orientation.z, _x.in_marker.pose.orientation.w, _x.in_marker.scale.x, _x.in_marker.scale.y, _x.in_marker.scale.z, _x.in_marker.color.r, _x.in_marker.color.g, _x.in_marker.color.b, _x.in_marker.color.a, _x.in_marker.lifetime.secs, _x.in_marker.lifetime.nsecs, _x.in_marker.frame_locked,) = _struct_3i10d4f2iB.unpack(str[start:end])
      self.in_marker.frame_locked = bool(self.in_marker.frame_locked)
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      self.in_marker.points = []
      for i in range(0, length):
        val1 = geometry_msgs.msg.Point()
        _x = val1
        start = end
        end += 24
        (_x.x, _x.y, _x.z,) = _struct_3d.unpack(str[start:end])
        self.in_marker.points.append(val1)
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      self.in_marker.colors = []
      for i in range(0, length):
        val1 = std_msgs.msg.ColorRGBA()
        _x = val1
        start = end
        end += 16
        (_x.r, _x.g, _x.b, _x.a,) = _struct_4f.unpack(str[start:end])
        self.in_marker.colors.append(val1)
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.in_marker.text = str[start:end].decode('utf-8')
      else:
        self.in_marker.text = str[start:end]
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.in_marker.mesh_resource = str[start:end].decode('utf-8')
      else:
        self.in_marker.mesh_resource = str[start:end]
      start = end
      end += 1
      (self.in_marker.mesh_use_embedded_materials,) = _struct_B.unpack(str[start:end])
      self.in_marker.mesh_use_embedded_materials = bool(self.in_marker.mesh_use_embedded_materials)
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e) #most likely buffer underfill


  def serialize_numpy(self, buff, numpy):
    """
    serialize message with numpy array types into buffer
    :param buff: buffer, ``StringIO``
    :param numpy: numpy python module
    """
    try:
      _x = self
      buff.write(_struct_3I.pack(_x.in_marker.header.seq, _x.in_marker.header.stamp.secs, _x.in_marker.header.stamp.nsecs))
      _x = self.in_marker.header.frame_id
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      if python3:
        buff.write(struct.pack('<I%sB'%length, length, *_x))
      else:
        buff.write(struct.pack('<I%ss'%length, length, _x))
      _x = self.in_marker.ns
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      if python3:
        buff.write(struct.pack('<I%sB'%length, length, *_x))
      else:
        buff.write(struct.pack('<I%ss'%length, length, _x))
      _x = self
      buff.write(_struct_3i10d4f2iB.pack(_x.in_marker.id, _x.in_marker.type, _x.in_marker.action, _x.in_marker.pose.position.x, _x.in_marker.pose.position.y, _x.in_marker.pose.position.z, _x.in_marker.pose.orientation.x, _x.in_marker.pose.orientation.y, _x.in_marker.pose.orientation.z, _x.in_marker.pose.orientation.w, _x.in_marker.scale.x, _x.in_marker.scale.y, _x.in_marker.scale.z, _x.in_marker.color.r, _x.in_marker.color.g, _x.in_marker.color.b, _x.in_marker.color.a, _x.in_marker.lifetime.secs, _x.in_marker.lifetime.nsecs, _x.in_marker.frame_locked))
      length = len(self.in_marker.points)
      buff.write(_struct_I.pack(length))
      for val1 in self.in_marker.points:
        _x = val1
        buff.write(_struct_3d.pack(_x.x, _x.y, _x.z))
      length = len(self.in_marker.colors)
      buff.write(_struct_I.pack(length))
      for val1 in self.in_marker.colors:
        _x = val1
        buff.write(_struct_4f.pack(_x.r, _x.g, _x.b, _x.a))
      _x = self.in_marker.text
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      if python3:
        buff.write(struct.pack('<I%sB'%length, length, *_x))
      else:
        buff.write(struct.pack('<I%ss'%length, length, _x))
      _x = self.in_marker.mesh_resource
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      if python3:
        buff.write(struct.pack('<I%sB'%length, length, *_x))
      else:
        buff.write(struct.pack('<I%ss'%length, length, _x))
      buff.write(_struct_B.pack(self.in_marker.mesh_use_embedded_materials))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(locals().get('_x', self)))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(locals().get('_x', self)))))

  def deserialize_numpy(self, str, numpy):
    """
    unpack serialized message in str into this message instance using numpy for array types
    :param str: byte array of serialized message, ``str``
    :param numpy: numpy python module
    """
    try:
      if self.in_marker is None:
        self.in_marker = visualization_msgs.msg.Marker()
      end = 0
      _x = self
      start = end
      end += 12
      (_x.in_marker.header.seq, _x.in_marker.header.stamp.secs, _x.in_marker.header.stamp.nsecs,) = _struct_3I.unpack(str[start:end])
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.in_marker.header.frame_id = str[start:end].decode('utf-8')
      else:
        self.in_marker.header.frame_id = str[start:end]
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.in_marker.ns = str[start:end].decode('utf-8')
      else:
        self.in_marker.ns = str[start:end]
      _x = self
      start = end
      end += 117
      (_x.in_marker.id, _x.in_marker.type, _x.in_marker.action, _x.in_marker.pose.position.x, _x.in_marker.pose.position.y, _x.in_marker.pose.position.z, _x.in_marker.pose.orientation.x, _x.in_marker.pose.orientation.y, _x.in_marker.pose.orientation.z, _x.in_marker.pose.orientation.w, _x.in_marker.scale.x, _x.in_marker.scale.y, _x.in_marker.scale.z, _x.in_marker.color.r, _x.in_marker.color.g, _x.in_marker.color.b, _x.in_marker.color.a, _x.in_marker.lifetime.secs, _x.in_marker.lifetime.nsecs, _x.in_marker.frame_locked,) = _struct_3i10d4f2iB.unpack(str[start:end])
      self.in_marker.frame_locked = bool(self.in_marker.frame_locked)
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      self.in_marker.points = []
      for i in range(0, length):
        val1 = geometry_msgs.msg.Point()
        _x = val1
        start = end
        end += 24
        (_x.x, _x.y, _x.z,) = _struct_3d.unpack(str[start:end])
        self.in_marker.points.append(val1)
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      self.in_marker.colors = []
      for i in range(0, length):
        val1 = std_msgs.msg.ColorRGBA()
        _x = val1
        start = end
        end += 16
        (_x.r, _x.g, _x.b, _x.a,) = _struct_4f.unpack(str[start:end])
        self.in_marker.colors.append(val1)
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.in_marker.text = str[start:end].decode('utf-8')
      else:
        self.in_marker.text = str[start:end]
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.in_marker.mesh_resource = str[start:end].decode('utf-8')
      else:
        self.in_marker.mesh_resource = str[start:end]
      start = end
      end += 1
      (self.in_marker.mesh_use_embedded_materials,) = _struct_B.unpack(str[start:end])
      self.in_marker.mesh_use_embedded_materials = bool(self.in_marker.mesh_use_embedded_materials)
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e) #most likely buffer underfill

_struct_I = genpy.struct_I
_struct_4f = struct.Struct("<4f")
_struct_3I = struct.Struct("<3I")
_struct_B = struct.Struct("<B")
_struct_3i10d4f2iB = struct.Struct("<3i10d4f2iB")
_struct_3d = struct.Struct("<3d")
# This Python file uses the following encoding: utf-8
"""autogenerated by genpy from box_messages/TransformMeResponse.msg. Do not edit."""
import sys
python3 = True if sys.hexversion > 0x03000000 else False
import genpy
import struct

import genpy
import geometry_msgs.msg
import visualization_msgs.msg
import std_msgs.msg

class TransformMeResponse(genpy.Message):
  _md5sum = "5c88b4fb2a28d60395f4b8d4ed7b1b69"
  _type = "box_messages/TransformMeResponse"
  _has_header = False #flag to mark the presence of a Header object
  _full_text = """visualization_msgs/Marker out_marker


================================================================================
MSG: visualization_msgs/Marker
# See http://www.ros.org/wiki/rviz/DisplayTypes/Marker and http://www.ros.org/wiki/rviz/Tutorials/Markers%3A%20Basic%20Shapes for more information on using this message with rviz

uint8 ARROW=0
uint8 CUBE=1
uint8 SPHERE=2
uint8 CYLINDER=3
uint8 LINE_STRIP=4
uint8 LINE_LIST=5
uint8 CUBE_LIST=6
uint8 SPHERE_LIST=7
uint8 POINTS=8
uint8 TEXT_VIEW_FACING=9
uint8 MESH_RESOURCE=10
uint8 TRIANGLE_LIST=11

uint8 ADD=0
uint8 MODIFY=0
uint8 DELETE=2
#uint8 DELETEALL=3 # TODO: enable for ROS-J, disabled for now but functionality is still there. Allows one to clear all markers in plugin

Header header                        # header for time/frame information
string ns                            # Namespace to place this object in... used in conjunction with id to create a unique name for the object
int32 id 		                         # object ID useful in conjunction with the namespace for manipulating and deleting the object later
int32 type 		                       # Type of object
int32 action 	                       # 0 add/modify an object, 1 (deprecated), 2 deletes an object, 3 deletes all objects
geometry_msgs/Pose pose                 # Pose of the object
geometry_msgs/Vector3 scale             # Scale of the object 1,1,1 means default (usually 1 meter square)
std_msgs/ColorRGBA color             # Color [0.0-1.0]
duration lifetime                    # How long the object should last before being automatically deleted.  0 means forever
bool frame_locked                    # If this marker should be frame-locked, i.e. retransformed into its frame every timestep

#Only used if the type specified has some use for them (eg. POINTS, LINE_STRIP, ...)
geometry_msgs/Point[] points
#Only used if the type specified has some use for them (eg. POINTS, LINE_STRIP, ...)
#number of colors must either be 0 or equal to the number of points
#NOTE: alpha is not yet used
std_msgs/ColorRGBA[] colors

# NOTE: only used for text markers
string text

# NOTE: only used for MESH_RESOURCE markers
string mesh_resource
bool mesh_use_embedded_materials

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

================================================================================
MSG: geometry_msgs/Pose
# A representation of pose in free space, composed of postion and orientation. 
Point position
Quaternion orientation

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

================================================================================
MSG: geometry_msgs/Quaternion
# This represents an orientation in free space in quaternion form.

float64 x
float64 y
float64 z
float64 w

================================================================================
MSG: geometry_msgs/Vector3
# This represents a vector in free space. 
# It is only meant to represent a direction. Therefore, it does not
# make sense to apply a translation to it (e.g., when applying a 
# generic rigid transformation to a Vector3, tf2 will only apply the
# rotation). If you want your data to be translatable too, use the
# geometry_msgs/Point message instead.

float64 x
float64 y
float64 z
================================================================================
MSG: std_msgs/ColorRGBA
float32 r
float32 g
float32 b
float32 a
"""
  __slots__ = ['out_marker']
  _slot_types = ['visualization_msgs/Marker']

  def __init__(self, *args, **kwds):
    """
    Constructor. Any message fields that are implicitly/explicitly
    set to None will be assigned a default value. The recommend
    use is keyword arguments as this is more robust to future message
    changes.  You cannot mix in-order arguments and keyword arguments.

    The available fields are:
       out_marker

    :param args: complete set of field values, in .msg order
    :param kwds: use keyword arguments corresponding to message field names
    to set specific fields.
    """
    if args or kwds:
      super(TransformMeResponse, self).__init__(*args, **kwds)
      #message fields cannot be None, assign default values for those that are
      if self.out_marker is None:
        self.out_marker = visualization_msgs.msg.Marker()
    else:
      self.out_marker = visualization_msgs.msg.Marker()

  def _get_types(self):
    """
    internal API method
    """
    return self._slot_types

  def serialize(self, buff):
    """
    serialize message into buffer
    :param buff: buffer, ``StringIO``
    """
    try:
      _x = self
      buff.write(_struct_3I.pack(_x.out_marker.header.seq, _x.out_marker.header.stamp.secs, _x.out_marker.header.stamp.nsecs))
      _x = self.out_marker.header.frame_id
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      if python3:
        buff.write(struct.pack('<I%sB'%length, length, *_x))
      else:
        buff.write(struct.pack('<I%ss'%length, length, _x))
      _x = self.out_marker.ns
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      if python3:
        buff.write(struct.pack('<I%sB'%length, length, *_x))
      else:
        buff.write(struct.pack('<I%ss'%length, length, _x))
      _x = self
      buff.write(_struct_3i10d4f2iB.pack(_x.out_marker.id, _x.out_marker.type, _x.out_marker.action, _x.out_marker.pose.position.x, _x.out_marker.pose.position.y, _x.out_marker.pose.position.z, _x.out_marker.pose.orientation.x, _x.out_marker.pose.orientation.y, _x.out_marker.pose.orientation.z, _x.out_marker.pose.orientation.w, _x.out_marker.scale.x, _x.out_marker.scale.y, _x.out_marker.scale.z, _x.out_marker.color.r, _x.out_marker.color.g, _x.out_marker.color.b, _x.out_marker.color.a, _x.out_marker.lifetime.secs, _x.out_marker.lifetime.nsecs, _x.out_marker.frame_locked))
      length = len(self.out_marker.points)
      buff.write(_struct_I.pack(length))
      for val1 in self.out_marker.points:
        _x = val1
        buff.write(_struct_3d.pack(_x.x, _x.y, _x.z))
      length = len(self.out_marker.colors)
      buff.write(_struct_I.pack(length))
      for val1 in self.out_marker.colors:
        _x = val1
        buff.write(_struct_4f.pack(_x.r, _x.g, _x.b, _x.a))
      _x = self.out_marker.text
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      if python3:
        buff.write(struct.pack('<I%sB'%length, length, *_x))
      else:
        buff.write(struct.pack('<I%ss'%length, length, _x))
      _x = self.out_marker.mesh_resource
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      if python3:
        buff.write(struct.pack('<I%sB'%length, length, *_x))
      else:
        buff.write(struct.pack('<I%ss'%length, length, _x))
      buff.write(_struct_B.pack(self.out_marker.mesh_use_embedded_materials))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(locals().get('_x', self)))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(locals().get('_x', self)))))

  def deserialize(self, str):
    """
    unpack serialized message in str into this message instance
    :param str: byte array of serialized message, ``str``
    """
    try:
      if self.out_marker is None:
        self.out_marker = visualization_msgs.msg.Marker()
      end = 0
      _x = self
      start = end
      end += 12
      (_x.out_marker.header.seq, _x.out_marker.header.stamp.secs, _x.out_marker.header.stamp.nsecs,) = _struct_3I.unpack(str[start:end])
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.out_marker.header.frame_id = str[start:end].decode('utf-8')
      else:
        self.out_marker.header.frame_id = str[start:end]
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.out_marker.ns = str[start:end].decode('utf-8')
      else:
        self.out_marker.ns = str[start:end]
      _x = self
      start = end
      end += 117
      (_x.out_marker.id, _x.out_marker.type, _x.out_marker.action, _x.out_marker.pose.position.x, _x.out_marker.pose.position.y, _x.out_marker.pose.position.z, _x.out_marker.pose.orientation.x, _x.out_marker.pose.orientation.y, _x.out_marker.pose.orientation.z, _x.out_marker.pose.orientation.w, _x.out_marker.scale.x, _x.out_marker.scale.y, _x.out_marker.scale.z, _x.out_marker.color.r, _x.out_marker.color.g, _x.out_marker.color.b, _x.out_marker.color.a, _x.out_marker.lifetime.secs, _x.out_marker.lifetime.nsecs, _x.out_marker.frame_locked,) = _struct_3i10d4f2iB.unpack(str[start:end])
      self.out_marker.frame_locked = bool(self.out_marker.frame_locked)
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      self.out_marker.points = []
      for i in range(0, length):
        val1 = geometry_msgs.msg.Point()
        _x = val1
        start = end
        end += 24
        (_x.x, _x.y, _x.z,) = _struct_3d.unpack(str[start:end])
        self.out_marker.points.append(val1)
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      self.out_marker.colors = []
      for i in range(0, length):
        val1 = std_msgs.msg.ColorRGBA()
        _x = val1
        start = end
        end += 16
        (_x.r, _x.g, _x.b, _x.a,) = _struct_4f.unpack(str[start:end])
        self.out_marker.colors.append(val1)
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.out_marker.text = str[start:end].decode('utf-8')
      else:
        self.out_marker.text = str[start:end]
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.out_marker.mesh_resource = str[start:end].decode('utf-8')
      else:
        self.out_marker.mesh_resource = str[start:end]
      start = end
      end += 1
      (self.out_marker.mesh_use_embedded_materials,) = _struct_B.unpack(str[start:end])
      self.out_marker.mesh_use_embedded_materials = bool(self.out_marker.mesh_use_embedded_materials)
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e) #most likely buffer underfill


  def serialize_numpy(self, buff, numpy):
    """
    serialize message with numpy array types into buffer
    :param buff: buffer, ``StringIO``
    :param numpy: numpy python module
    """
    try:
      _x = self
      buff.write(_struct_3I.pack(_x.out_marker.header.seq, _x.out_marker.header.stamp.secs, _x.out_marker.header.stamp.nsecs))
      _x = self.out_marker.header.frame_id
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      if python3:
        buff.write(struct.pack('<I%sB'%length, length, *_x))
      else:
        buff.write(struct.pack('<I%ss'%length, length, _x))
      _x = self.out_marker.ns
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      if python3:
        buff.write(struct.pack('<I%sB'%length, length, *_x))
      else:
        buff.write(struct.pack('<I%ss'%length, length, _x))
      _x = self
      buff.write(_struct_3i10d4f2iB.pack(_x.out_marker.id, _x.out_marker.type, _x.out_marker.action, _x.out_marker.pose.position.x, _x.out_marker.pose.position.y, _x.out_marker.pose.position.z, _x.out_marker.pose.orientation.x, _x.out_marker.pose.orientation.y, _x.out_marker.pose.orientation.z, _x.out_marker.pose.orientation.w, _x.out_marker.scale.x, _x.out_marker.scale.y, _x.out_marker.scale.z, _x.out_marker.color.r, _x.out_marker.color.g, _x.out_marker.color.b, _x.out_marker.color.a, _x.out_marker.lifetime.secs, _x.out_marker.lifetime.nsecs, _x.out_marker.frame_locked))
      length = len(self.out_marker.points)
      buff.write(_struct_I.pack(length))
      for val1 in self.out_marker.points:
        _x = val1
        buff.write(_struct_3d.pack(_x.x, _x.y, _x.z))
      length = len(self.out_marker.colors)
      buff.write(_struct_I.pack(length))
      for val1 in self.out_marker.colors:
        _x = val1
        buff.write(_struct_4f.pack(_x.r, _x.g, _x.b, _x.a))
      _x = self.out_marker.text
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      if python3:
        buff.write(struct.pack('<I%sB'%length, length, *_x))
      else:
        buff.write(struct.pack('<I%ss'%length, length, _x))
      _x = self.out_marker.mesh_resource
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      if python3:
        buff.write(struct.pack('<I%sB'%length, length, *_x))
      else:
        buff.write(struct.pack('<I%ss'%length, length, _x))
      buff.write(_struct_B.pack(self.out_marker.mesh_use_embedded_materials))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(locals().get('_x', self)))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(locals().get('_x', self)))))

  def deserialize_numpy(self, str, numpy):
    """
    unpack serialized message in str into this message instance using numpy for array types
    :param str: byte array of serialized message, ``str``
    :param numpy: numpy python module
    """
    try:
      if self.out_marker is None:
        self.out_marker = visualization_msgs.msg.Marker()
      end = 0
      _x = self
      start = end
      end += 12
      (_x.out_marker.header.seq, _x.out_marker.header.stamp.secs, _x.out_marker.header.stamp.nsecs,) = _struct_3I.unpack(str[start:end])
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.out_marker.header.frame_id = str[start:end].decode('utf-8')
      else:
        self.out_marker.header.frame_id = str[start:end]
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.out_marker.ns = str[start:end].decode('utf-8')
      else:
        self.out_marker.ns = str[start:end]
      _x = self
      start = end
      end += 117
      (_x.out_marker.id, _x.out_marker.type, _x.out_marker.action, _x.out_marker.pose.position.x, _x.out_marker.pose.position.y, _x.out_marker.pose.position.z, _x.out_marker.pose.orientation.x, _x.out_marker.pose.orientation.y, _x.out_marker.pose.orientation.z, _x.out_marker.pose.orientation.w, _x.out_marker.scale.x, _x.out_marker.scale.y, _x.out_marker.scale.z, _x.out_marker.color.r, _x.out_marker.color.g, _x.out_marker.color.b, _x.out_marker.color.a, _x.out_marker.lifetime.secs, _x.out_marker.lifetime.nsecs, _x.out_marker.frame_locked,) = _struct_3i10d4f2iB.unpack(str[start:end])
      self.out_marker.frame_locked = bool(self.out_marker.frame_locked)
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      self.out_marker.points = []
      for i in range(0, length):
        val1 = geometry_msgs.msg.Point()
        _x = val1
        start = end
        end += 24
        (_x.x, _x.y, _x.z,) = _struct_3d.unpack(str[start:end])
        self.out_marker.points.append(val1)
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      self.out_marker.colors = []
      for i in range(0, length):
        val1 = std_msgs.msg.ColorRGBA()
        _x = val1
        start = end
        end += 16
        (_x.r, _x.g, _x.b, _x.a,) = _struct_4f.unpack(str[start:end])
        self.out_marker.colors.append(val1)
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.out_marker.text = str[start:end].decode('utf-8')
      else:
        self.out_marker.text = str[start:end]
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.out_marker.mesh_resource = str[start:end].decode('utf-8')
      else:
        self.out_marker.mesh_resource = str[start:end]
      start = end
      end += 1
      (self.out_marker.mesh_use_embedded_materials,) = _struct_B.unpack(str[start:end])
      self.out_marker.mesh_use_embedded_materials = bool(self.out_marker.mesh_use_embedded_materials)
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e) #most likely buffer underfill

_struct_I = genpy.struct_I
_struct_4f = struct.Struct("<4f")
_struct_3I = struct.Struct("<3I")
_struct_B = struct.Struct("<B")
_struct_3i10d4f2iB = struct.Struct("<3i10d4f2iB")
_struct_3d = struct.Struct("<3d")
class TransformMe(object):
  _type          = 'box_messages/TransformMe'
  _md5sum = '6b1dc27799a5e4b8601bf281f71c94ee'
  _request_class  = TransformMeRequest
  _response_class = TransformMeResponse
