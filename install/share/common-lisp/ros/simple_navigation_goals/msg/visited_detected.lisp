; Auto-generated. Do not edit!


(cl:in-package simple_navigation_goals-msg)


;//! \htmlinclude visited_detected.msg.html

(cl:defclass <visited_detected> (roslisp-msg-protocol:ros-message)
  ((visited
    :reader visited
    :initarg :visited
    :type cl:integer
    :initform 0)
   (detected
    :reader detected
    :initarg :detected
    :type cl:integer
    :initform 0))
)

(cl:defclass visited_detected (<visited_detected>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <visited_detected>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'visited_detected)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name simple_navigation_goals-msg:<visited_detected> is deprecated: use simple_navigation_goals-msg:visited_detected instead.")))

(cl:ensure-generic-function 'visited-val :lambda-list '(m))
(cl:defmethod visited-val ((m <visited_detected>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader simple_navigation_goals-msg:visited-val is deprecated.  Use simple_navigation_goals-msg:visited instead.")
  (visited m))

(cl:ensure-generic-function 'detected-val :lambda-list '(m))
(cl:defmethod detected-val ((m <visited_detected>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader simple_navigation_goals-msg:detected-val is deprecated.  Use simple_navigation_goals-msg:detected instead.")
  (detected m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <visited_detected>) ostream)
  "Serializes a message object of type '<visited_detected>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'visited)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'visited)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'visited)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'visited)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'detected)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'detected)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'detected)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'detected)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <visited_detected>) istream)
  "Deserializes a message object of type '<visited_detected>"
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'visited)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'visited)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'visited)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'visited)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'detected)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'detected)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'detected)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'detected)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<visited_detected>)))
  "Returns string type for a message object of type '<visited_detected>"
  "simple_navigation_goals/visited_detected")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'visited_detected)))
  "Returns string type for a message object of type 'visited_detected"
  "simple_navigation_goals/visited_detected")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<visited_detected>)))
  "Returns md5sum for a message object of type '<visited_detected>"
  "1528d66728994c0671d44375b363822a")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'visited_detected)))
  "Returns md5sum for a message object of type 'visited_detected"
  "1528d66728994c0671d44375b363822a")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<visited_detected>)))
  "Returns full string definition for message of type '<visited_detected>"
  (cl:format cl:nil "uint32 visited~%uint32 detected~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'visited_detected)))
  "Returns full string definition for message of type 'visited_detected"
  (cl:format cl:nil "uint32 visited~%uint32 detected~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <visited_detected>))
  (cl:+ 0
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <visited_detected>))
  "Converts a ROS message object to a list"
  (cl:list 'visited_detected
    (cl:cons ':visited (visited msg))
    (cl:cons ':detected (detected msg))
))
