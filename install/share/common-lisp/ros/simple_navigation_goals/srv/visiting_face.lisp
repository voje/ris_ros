; Auto-generated. Do not edit!


(cl:in-package simple_navigation_goals-srv)


;//! \htmlinclude visiting_face-request.msg.html

(cl:defclass <visiting_face-request> (roslisp-msg-protocol:ros-message)
  ((x
    :reader x
    :initarg :x
    :type cl:float
    :initform 0.0)
   (y
    :reader y
    :initarg :y
    :type cl:float
    :initform 0.0))
)

(cl:defclass visiting_face-request (<visiting_face-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <visiting_face-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'visiting_face-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name simple_navigation_goals-srv:<visiting_face-request> is deprecated: use simple_navigation_goals-srv:visiting_face-request instead.")))

(cl:ensure-generic-function 'x-val :lambda-list '(m))
(cl:defmethod x-val ((m <visiting_face-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader simple_navigation_goals-srv:x-val is deprecated.  Use simple_navigation_goals-srv:x instead.")
  (x m))

(cl:ensure-generic-function 'y-val :lambda-list '(m))
(cl:defmethod y-val ((m <visiting_face-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader simple_navigation_goals-srv:y-val is deprecated.  Use simple_navigation_goals-srv:y instead.")
  (y m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <visiting_face-request>) ostream)
  "Serializes a message object of type '<visiting_face-request>"
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'x))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'y))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <visiting_face-request>) istream)
  "Deserializes a message object of type '<visiting_face-request>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'x) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'y) (roslisp-utils:decode-double-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<visiting_face-request>)))
  "Returns string type for a service object of type '<visiting_face-request>"
  "simple_navigation_goals/visiting_faceRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'visiting_face-request)))
  "Returns string type for a service object of type 'visiting_face-request"
  "simple_navigation_goals/visiting_faceRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<visiting_face-request>)))
  "Returns md5sum for a message object of type '<visiting_face-request>"
  "ac13b6adac2b3607e32616bbe20e9153")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'visiting_face-request)))
  "Returns md5sum for a message object of type 'visiting_face-request"
  "ac13b6adac2b3607e32616bbe20e9153")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<visiting_face-request>)))
  "Returns full string definition for message of type '<visiting_face-request>"
  (cl:format cl:nil "float64 x~%float64 y~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'visiting_face-request)))
  "Returns full string definition for message of type 'visiting_face-request"
  (cl:format cl:nil "float64 x~%float64 y~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <visiting_face-request>))
  (cl:+ 0
     8
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <visiting_face-request>))
  "Converts a ROS message object to a list"
  (cl:list 'visiting_face-request
    (cl:cons ':x (x msg))
    (cl:cons ':y (y msg))
))
;//! \htmlinclude visiting_face-response.msg.html

(cl:defclass <visiting_face-response> (roslisp-msg-protocol:ros-message)
  ((odgovor
    :reader odgovor
    :initarg :odgovor
    :type cl:integer
    :initform 0))
)

(cl:defclass visiting_face-response (<visiting_face-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <visiting_face-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'visiting_face-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name simple_navigation_goals-srv:<visiting_face-response> is deprecated: use simple_navigation_goals-srv:visiting_face-response instead.")))

(cl:ensure-generic-function 'odgovor-val :lambda-list '(m))
(cl:defmethod odgovor-val ((m <visiting_face-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader simple_navigation_goals-srv:odgovor-val is deprecated.  Use simple_navigation_goals-srv:odgovor instead.")
  (odgovor m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <visiting_face-response>) ostream)
  "Serializes a message object of type '<visiting_face-response>"
  (cl:let* ((signed (cl:slot-value msg 'odgovor)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <visiting_face-response>) istream)
  "Deserializes a message object of type '<visiting_face-response>"
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'odgovor) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<visiting_face-response>)))
  "Returns string type for a service object of type '<visiting_face-response>"
  "simple_navigation_goals/visiting_faceResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'visiting_face-response)))
  "Returns string type for a service object of type 'visiting_face-response"
  "simple_navigation_goals/visiting_faceResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<visiting_face-response>)))
  "Returns md5sum for a message object of type '<visiting_face-response>"
  "ac13b6adac2b3607e32616bbe20e9153")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'visiting_face-response)))
  "Returns md5sum for a message object of type 'visiting_face-response"
  "ac13b6adac2b3607e32616bbe20e9153")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<visiting_face-response>)))
  "Returns full string definition for message of type '<visiting_face-response>"
  (cl:format cl:nil "int32 odgovor~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'visiting_face-response)))
  "Returns full string definition for message of type 'visiting_face-response"
  (cl:format cl:nil "int32 odgovor~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <visiting_face-response>))
  (cl:+ 0
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <visiting_face-response>))
  "Converts a ROS message object to a list"
  (cl:list 'visiting_face-response
    (cl:cons ':odgovor (odgovor msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'visiting_face)))
  'visiting_face-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'visiting_face)))
  'visiting_face-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'visiting_face)))
  "Returns string type for a service object of type '<visiting_face>"
  "simple_navigation_goals/visiting_face")