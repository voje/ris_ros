
(cl:in-package :asdf)

(defsystem "simple_navigation_goals-srv"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "reached" :depends-on ("_package_reached"))
    (:file "_package_reached" :depends-on ("_package"))
    (:file "visiting_face" :depends-on ("_package_visiting_face"))
    (:file "_package_visiting_face" :depends-on ("_package"))
  ))