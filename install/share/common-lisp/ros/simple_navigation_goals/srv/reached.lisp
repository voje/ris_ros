; Auto-generated. Do not edit!


(cl:in-package simple_navigation_goals-srv)


;//! \htmlinclude reached-request.msg.html

(cl:defclass <reached-request> (roslisp-msg-protocol:ros-message)
  ((x
    :reader x
    :initarg :x
    :type cl:float
    :initform 0.0)
   (y
    :reader y
    :initarg :y
    :type cl:float
    :initform 0.0))
)

(cl:defclass reached-request (<reached-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <reached-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'reached-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name simple_navigation_goals-srv:<reached-request> is deprecated: use simple_navigation_goals-srv:reached-request instead.")))

(cl:ensure-generic-function 'x-val :lambda-list '(m))
(cl:defmethod x-val ((m <reached-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader simple_navigation_goals-srv:x-val is deprecated.  Use simple_navigation_goals-srv:x instead.")
  (x m))

(cl:ensure-generic-function 'y-val :lambda-list '(m))
(cl:defmethod y-val ((m <reached-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader simple_navigation_goals-srv:y-val is deprecated.  Use simple_navigation_goals-srv:y instead.")
  (y m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <reached-request>) ostream)
  "Serializes a message object of type '<reached-request>"
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'x))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'y))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <reached-request>) istream)
  "Deserializes a message object of type '<reached-request>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'x) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'y) (roslisp-utils:decode-double-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<reached-request>)))
  "Returns string type for a service object of type '<reached-request>"
  "simple_navigation_goals/reachedRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'reached-request)))
  "Returns string type for a service object of type 'reached-request"
  "simple_navigation_goals/reachedRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<reached-request>)))
  "Returns md5sum for a message object of type '<reached-request>"
  "209f516d3eb691f0663e25cb750d67c1")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'reached-request)))
  "Returns md5sum for a message object of type 'reached-request"
  "209f516d3eb691f0663e25cb750d67c1")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<reached-request>)))
  "Returns full string definition for message of type '<reached-request>"
  (cl:format cl:nil "float64 x~%float64 y~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'reached-request)))
  "Returns full string definition for message of type 'reached-request"
  (cl:format cl:nil "float64 x~%float64 y~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <reached-request>))
  (cl:+ 0
     8
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <reached-request>))
  "Converts a ROS message object to a list"
  (cl:list 'reached-request
    (cl:cons ':x (x msg))
    (cl:cons ':y (y msg))
))
;//! \htmlinclude reached-response.msg.html

(cl:defclass <reached-response> (roslisp-msg-protocol:ros-message)
  ()
)

(cl:defclass reached-response (<reached-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <reached-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'reached-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name simple_navigation_goals-srv:<reached-response> is deprecated: use simple_navigation_goals-srv:reached-response instead.")))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <reached-response>) ostream)
  "Serializes a message object of type '<reached-response>"
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <reached-response>) istream)
  "Deserializes a message object of type '<reached-response>"
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<reached-response>)))
  "Returns string type for a service object of type '<reached-response>"
  "simple_navigation_goals/reachedResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'reached-response)))
  "Returns string type for a service object of type 'reached-response"
  "simple_navigation_goals/reachedResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<reached-response>)))
  "Returns md5sum for a message object of type '<reached-response>"
  "209f516d3eb691f0663e25cb750d67c1")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'reached-response)))
  "Returns md5sum for a message object of type 'reached-response"
  "209f516d3eb691f0663e25cb750d67c1")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<reached-response>)))
  "Returns full string definition for message of type '<reached-response>"
  (cl:format cl:nil "~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'reached-response)))
  "Returns full string definition for message of type 'reached-response"
  (cl:format cl:nil "~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <reached-response>))
  (cl:+ 0
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <reached-response>))
  "Converts a ROS message object to a list"
  (cl:list 'reached-response
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'reached)))
  'reached-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'reached)))
  'reached-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'reached)))
  "Returns string type for a service object of type '<reached>"
  "simple_navigation_goals/reached")