
(cl:in-package :asdf)

(defsystem "box-srv"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "Generic" :depends-on ("_package_Generic"))
    (:file "_package_Generic" :depends-on ("_package"))
  ))