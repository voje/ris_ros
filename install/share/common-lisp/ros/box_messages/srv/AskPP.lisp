; Auto-generated. Do not edit!


(cl:in-package box_messages-srv)


;//! \htmlinclude AskPP-request.msg.html

(cl:defclass <AskPP-request> (roslisp-msg-protocol:ros-message)
  ((str1
    :reader str1
    :initarg :str1
    :type cl:string
    :initform ""))
)

(cl:defclass AskPP-request (<AskPP-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <AskPP-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'AskPP-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name box_messages-srv:<AskPP-request> is deprecated: use box_messages-srv:AskPP-request instead.")))

(cl:ensure-generic-function 'str1-val :lambda-list '(m))
(cl:defmethod str1-val ((m <AskPP-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader box_messages-srv:str1-val is deprecated.  Use box_messages-srv:str1 instead.")
  (str1 m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <AskPP-request>) ostream)
  "Serializes a message object of type '<AskPP-request>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'str1))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'str1))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <AskPP-request>) istream)
  "Deserializes a message object of type '<AskPP-request>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'str1) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'str1) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<AskPP-request>)))
  "Returns string type for a service object of type '<AskPP-request>"
  "box_messages/AskPPRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'AskPP-request)))
  "Returns string type for a service object of type 'AskPP-request"
  "box_messages/AskPPRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<AskPP-request>)))
  "Returns md5sum for a message object of type '<AskPP-request>"
  "af9e4ccc9634b88d2c3d5ac3b7231fd2")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'AskPP-request)))
  "Returns md5sum for a message object of type 'AskPP-request"
  "af9e4ccc9634b88d2c3d5ac3b7231fd2")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<AskPP-request>)))
  "Returns full string definition for message of type '<AskPP-request>"
  (cl:format cl:nil "string str1~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'AskPP-request)))
  "Returns full string definition for message of type 'AskPP-request"
  (cl:format cl:nil "string str1~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <AskPP-request>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'str1))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <AskPP-request>))
  "Converts a ROS message object to a list"
  (cl:list 'AskPP-request
    (cl:cons ':str1 (str1 msg))
))
;//! \htmlinclude AskPP-response.msg.html

(cl:defclass <AskPP-response> (roslisp-msg-protocol:ros-message)
  ((marker1
    :reader marker1
    :initarg :marker1
    :type visualization_msgs-msg:Marker
    :initform (cl:make-instance 'visualization_msgs-msg:Marker)))
)

(cl:defclass AskPP-response (<AskPP-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <AskPP-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'AskPP-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name box_messages-srv:<AskPP-response> is deprecated: use box_messages-srv:AskPP-response instead.")))

(cl:ensure-generic-function 'marker1-val :lambda-list '(m))
(cl:defmethod marker1-val ((m <AskPP-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader box_messages-srv:marker1-val is deprecated.  Use box_messages-srv:marker1 instead.")
  (marker1 m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <AskPP-response>) ostream)
  "Serializes a message object of type '<AskPP-response>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'marker1) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <AskPP-response>) istream)
  "Deserializes a message object of type '<AskPP-response>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'marker1) istream)
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<AskPP-response>)))
  "Returns string type for a service object of type '<AskPP-response>"
  "box_messages/AskPPResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'AskPP-response)))
  "Returns string type for a service object of type 'AskPP-response"
  "box_messages/AskPPResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<AskPP-response>)))
  "Returns md5sum for a message object of type '<AskPP-response>"
  "af9e4ccc9634b88d2c3d5ac3b7231fd2")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'AskPP-response)))
  "Returns md5sum for a message object of type 'AskPP-response"
  "af9e4ccc9634b88d2c3d5ac3b7231fd2")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<AskPP-response>)))
  "Returns full string definition for message of type '<AskPP-response>"
  (cl:format cl:nil "visualization_msgs/Marker marker1~%~%================================================================================~%MSG: visualization_msgs/Marker~%# See http://www.ros.org/wiki/rviz/DisplayTypes/Marker and http://www.ros.org/wiki/rviz/Tutorials/Markers%3A%20Basic%20Shapes for more information on using this message with rviz~%~%uint8 ARROW=0~%uint8 CUBE=1~%uint8 SPHERE=2~%uint8 CYLINDER=3~%uint8 LINE_STRIP=4~%uint8 LINE_LIST=5~%uint8 CUBE_LIST=6~%uint8 SPHERE_LIST=7~%uint8 POINTS=8~%uint8 TEXT_VIEW_FACING=9~%uint8 MESH_RESOURCE=10~%uint8 TRIANGLE_LIST=11~%~%uint8 ADD=0~%uint8 MODIFY=0~%uint8 DELETE=2~%#uint8 DELETEALL=3 # TODO: enable for ROS-J, disabled for now but functionality is still there. Allows one to clear all markers in plugin~%~%Header header                        # header for time/frame information~%string ns                            # Namespace to place this object in... used in conjunction with id to create a unique name for the object~%int32 id 		                         # object ID useful in conjunction with the namespace for manipulating and deleting the object later~%int32 type 		                       # Type of object~%int32 action 	                       # 0 add/modify an object, 1 (deprecated), 2 deletes an object, 3 deletes all objects~%geometry_msgs/Pose pose                 # Pose of the object~%geometry_msgs/Vector3 scale             # Scale of the object 1,1,1 means default (usually 1 meter square)~%std_msgs/ColorRGBA color             # Color [0.0-1.0]~%duration lifetime                    # How long the object should last before being automatically deleted.  0 means forever~%bool frame_locked                    # If this marker should be frame-locked, i.e. retransformed into its frame every timestep~%~%#Only used if the type specified has some use for them (eg. POINTS, LINE_STRIP, ...)~%geometry_msgs/Point[] points~%#Only used if the type specified has some use for them (eg. POINTS, LINE_STRIP, ...)~%#number of colors must either be 0 or equal to the number of points~%#NOTE: alpha is not yet used~%std_msgs/ColorRGBA[] colors~%~%# NOTE: only used for text markers~%string text~%~%# NOTE: only used for MESH_RESOURCE markers~%string mesh_resource~%bool mesh_use_embedded_materials~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of postion and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%================================================================================~%MSG: geometry_msgs/Vector3~%# This represents a vector in free space. ~%# It is only meant to represent a direction. Therefore, it does not~%# make sense to apply a translation to it (e.g., when applying a ~%# generic rigid transformation to a Vector3, tf2 will only apply the~%# rotation). If you want your data to be translatable too, use the~%# geometry_msgs/Point message instead.~%~%float64 x~%float64 y~%float64 z~%================================================================================~%MSG: std_msgs/ColorRGBA~%float32 r~%float32 g~%float32 b~%float32 a~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'AskPP-response)))
  "Returns full string definition for message of type 'AskPP-response"
  (cl:format cl:nil "visualization_msgs/Marker marker1~%~%================================================================================~%MSG: visualization_msgs/Marker~%# See http://www.ros.org/wiki/rviz/DisplayTypes/Marker and http://www.ros.org/wiki/rviz/Tutorials/Markers%3A%20Basic%20Shapes for more information on using this message with rviz~%~%uint8 ARROW=0~%uint8 CUBE=1~%uint8 SPHERE=2~%uint8 CYLINDER=3~%uint8 LINE_STRIP=4~%uint8 LINE_LIST=5~%uint8 CUBE_LIST=6~%uint8 SPHERE_LIST=7~%uint8 POINTS=8~%uint8 TEXT_VIEW_FACING=9~%uint8 MESH_RESOURCE=10~%uint8 TRIANGLE_LIST=11~%~%uint8 ADD=0~%uint8 MODIFY=0~%uint8 DELETE=2~%#uint8 DELETEALL=3 # TODO: enable for ROS-J, disabled for now but functionality is still there. Allows one to clear all markers in plugin~%~%Header header                        # header for time/frame information~%string ns                            # Namespace to place this object in... used in conjunction with id to create a unique name for the object~%int32 id 		                         # object ID useful in conjunction with the namespace for manipulating and deleting the object later~%int32 type 		                       # Type of object~%int32 action 	                       # 0 add/modify an object, 1 (deprecated), 2 deletes an object, 3 deletes all objects~%geometry_msgs/Pose pose                 # Pose of the object~%geometry_msgs/Vector3 scale             # Scale of the object 1,1,1 means default (usually 1 meter square)~%std_msgs/ColorRGBA color             # Color [0.0-1.0]~%duration lifetime                    # How long the object should last before being automatically deleted.  0 means forever~%bool frame_locked                    # If this marker should be frame-locked, i.e. retransformed into its frame every timestep~%~%#Only used if the type specified has some use for them (eg. POINTS, LINE_STRIP, ...)~%geometry_msgs/Point[] points~%#Only used if the type specified has some use for them (eg. POINTS, LINE_STRIP, ...)~%#number of colors must either be 0 or equal to the number of points~%#NOTE: alpha is not yet used~%std_msgs/ColorRGBA[] colors~%~%# NOTE: only used for text markers~%string text~%~%# NOTE: only used for MESH_RESOURCE markers~%string mesh_resource~%bool mesh_use_embedded_materials~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of postion and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%================================================================================~%MSG: geometry_msgs/Vector3~%# This represents a vector in free space. ~%# It is only meant to represent a direction. Therefore, it does not~%# make sense to apply a translation to it (e.g., when applying a ~%# generic rigid transformation to a Vector3, tf2 will only apply the~%# rotation). If you want your data to be translatable too, use the~%# geometry_msgs/Point message instead.~%~%float64 x~%float64 y~%float64 z~%================================================================================~%MSG: std_msgs/ColorRGBA~%float32 r~%float32 g~%float32 b~%float32 a~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <AskPP-response>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'marker1))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <AskPP-response>))
  "Converts a ROS message object to a list"
  (cl:list 'AskPP-response
    (cl:cons ':marker1 (marker1 msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'AskPP)))
  'AskPP-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'AskPP)))
  'AskPP-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'AskPP)))
  "Returns string type for a service object of type '<AskPP>"
  "box_messages/AskPP")