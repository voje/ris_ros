
(cl:in-package :asdf)

(defsystem "box_messages-srv"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :visualization_msgs-msg
)
  :components ((:file "_package")
    (:file "TransformMe" :depends-on ("_package_TransformMe"))
    (:file "_package_TransformMe" :depends-on ("_package"))
    (:file "AskPP" :depends-on ("_package_AskPP"))
    (:file "_package_AskPP" :depends-on ("_package"))
    (:file "Surround" :depends-on ("_package_Surround"))
    (:file "_package_Surround" :depends-on ("_package"))
    (:file "Generic" :depends-on ("_package_Generic"))
    (:file "_package_Generic" :depends-on ("_package"))
  ))