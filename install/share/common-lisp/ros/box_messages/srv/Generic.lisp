; Auto-generated. Do not edit!


(cl:in-package box_messages-srv)


;//! \htmlinclude Generic-request.msg.html

(cl:defclass <Generic-request> (roslisp-msg-protocol:ros-message)
  ((str1
    :reader str1
    :initarg :str1
    :type cl:string
    :initform "")
   (str2
    :reader str2
    :initarg :str2
    :type cl:string
    :initform "")
   (int1
    :reader int1
    :initarg :int1
    :type cl:integer
    :initform 0)
   (int2
    :reader int2
    :initarg :int2
    :type cl:integer
    :initform 0))
)

(cl:defclass Generic-request (<Generic-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Generic-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Generic-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name box_messages-srv:<Generic-request> is deprecated: use box_messages-srv:Generic-request instead.")))

(cl:ensure-generic-function 'str1-val :lambda-list '(m))
(cl:defmethod str1-val ((m <Generic-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader box_messages-srv:str1-val is deprecated.  Use box_messages-srv:str1 instead.")
  (str1 m))

(cl:ensure-generic-function 'str2-val :lambda-list '(m))
(cl:defmethod str2-val ((m <Generic-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader box_messages-srv:str2-val is deprecated.  Use box_messages-srv:str2 instead.")
  (str2 m))

(cl:ensure-generic-function 'int1-val :lambda-list '(m))
(cl:defmethod int1-val ((m <Generic-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader box_messages-srv:int1-val is deprecated.  Use box_messages-srv:int1 instead.")
  (int1 m))

(cl:ensure-generic-function 'int2-val :lambda-list '(m))
(cl:defmethod int2-val ((m <Generic-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader box_messages-srv:int2-val is deprecated.  Use box_messages-srv:int2 instead.")
  (int2 m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Generic-request>) ostream)
  "Serializes a message object of type '<Generic-request>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'str1))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'str1))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'str2))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'str2))
  (cl:let* ((signed (cl:slot-value msg 'int1)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'int2)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Generic-request>) istream)
  "Deserializes a message object of type '<Generic-request>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'str1) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'str1) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'str2) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'str2) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'int1) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'int2) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Generic-request>)))
  "Returns string type for a service object of type '<Generic-request>"
  "box_messages/GenericRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Generic-request)))
  "Returns string type for a service object of type 'Generic-request"
  "box_messages/GenericRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Generic-request>)))
  "Returns md5sum for a message object of type '<Generic-request>"
  "e470ed8f1ebb5576fe63513c2de1b31b")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Generic-request)))
  "Returns md5sum for a message object of type 'Generic-request"
  "e470ed8f1ebb5576fe63513c2de1b31b")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Generic-request>)))
  "Returns full string definition for message of type '<Generic-request>"
  (cl:format cl:nil "string str1~%string str2~%int32 int1~%int32 int2~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Generic-request)))
  "Returns full string definition for message of type 'Generic-request"
  (cl:format cl:nil "string str1~%string str2~%int32 int1~%int32 int2~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Generic-request>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'str1))
     4 (cl:length (cl:slot-value msg 'str2))
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Generic-request>))
  "Converts a ROS message object to a list"
  (cl:list 'Generic-request
    (cl:cons ':str1 (str1 msg))
    (cl:cons ':str2 (str2 msg))
    (cl:cons ':int1 (int1 msg))
    (cl:cons ':int2 (int2 msg))
))
;//! \htmlinclude Generic-response.msg.html

(cl:defclass <Generic-response> (roslisp-msg-protocol:ros-message)
  ((str3
    :reader str3
    :initarg :str3
    :type cl:string
    :initform "")
   (str4
    :reader str4
    :initarg :str4
    :type cl:string
    :initform "")
   (int3
    :reader int3
    :initarg :int3
    :type cl:integer
    :initform 0)
   (int4
    :reader int4
    :initarg :int4
    :type cl:integer
    :initform 0))
)

(cl:defclass Generic-response (<Generic-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Generic-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Generic-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name box_messages-srv:<Generic-response> is deprecated: use box_messages-srv:Generic-response instead.")))

(cl:ensure-generic-function 'str3-val :lambda-list '(m))
(cl:defmethod str3-val ((m <Generic-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader box_messages-srv:str3-val is deprecated.  Use box_messages-srv:str3 instead.")
  (str3 m))

(cl:ensure-generic-function 'str4-val :lambda-list '(m))
(cl:defmethod str4-val ((m <Generic-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader box_messages-srv:str4-val is deprecated.  Use box_messages-srv:str4 instead.")
  (str4 m))

(cl:ensure-generic-function 'int3-val :lambda-list '(m))
(cl:defmethod int3-val ((m <Generic-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader box_messages-srv:int3-val is deprecated.  Use box_messages-srv:int3 instead.")
  (int3 m))

(cl:ensure-generic-function 'int4-val :lambda-list '(m))
(cl:defmethod int4-val ((m <Generic-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader box_messages-srv:int4-val is deprecated.  Use box_messages-srv:int4 instead.")
  (int4 m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Generic-response>) ostream)
  "Serializes a message object of type '<Generic-response>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'str3))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'str3))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'str4))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'str4))
  (cl:let* ((signed (cl:slot-value msg 'int3)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'int4)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Generic-response>) istream)
  "Deserializes a message object of type '<Generic-response>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'str3) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'str3) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'str4) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'str4) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'int3) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'int4) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Generic-response>)))
  "Returns string type for a service object of type '<Generic-response>"
  "box_messages/GenericResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Generic-response)))
  "Returns string type for a service object of type 'Generic-response"
  "box_messages/GenericResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Generic-response>)))
  "Returns md5sum for a message object of type '<Generic-response>"
  "e470ed8f1ebb5576fe63513c2de1b31b")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Generic-response)))
  "Returns md5sum for a message object of type 'Generic-response"
  "e470ed8f1ebb5576fe63513c2de1b31b")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Generic-response>)))
  "Returns full string definition for message of type '<Generic-response>"
  (cl:format cl:nil "string str3~%string str4~%int32 int3~%int32 int4~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Generic-response)))
  "Returns full string definition for message of type 'Generic-response"
  (cl:format cl:nil "string str3~%string str4~%int32 int3~%int32 int4~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Generic-response>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'str3))
     4 (cl:length (cl:slot-value msg 'str4))
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Generic-response>))
  "Converts a ROS message object to a list"
  (cl:list 'Generic-response
    (cl:cons ':str3 (str3 msg))
    (cl:cons ':str4 (str4 msg))
    (cl:cons ':int3 (int3 msg))
    (cl:cons ':int4 (int4 msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'Generic)))
  'Generic-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'Generic)))
  'Generic-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Generic)))
  "Returns string type for a service object of type '<Generic>"
  "box_messages/Generic")