
(cl:in-package :asdf)

(defsystem "face_clustering-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "visited_detected" :depends-on ("_package_visited_detected"))
    (:file "_package_visited_detected" :depends-on ("_package"))
  ))