#when in $HOME
echo "Wait a few momens, for the workspace to build."
cd ~ &&
if [ -d "ROS" ]; then
	mv ROS ROS_original;
fi
mv ris_ros ROS &&
rm ./ROS/src/CMakeLists.txt &&
cd ./ROS/src &&
catkin_init_workspace &&
cd .. &&
catkin_make &&
source ./devel/setup.bash &&
echo "### ROS is ready for usage ###"
