#Roomba Turtlebot

ROS (Robot Operating System) project. 
Implemented on Turtlebot (low-cost, personal robot kit with open-source software). 

The map needs to be mapped manually. 
The robot is able to perform the following tasks: 

* perform a walk through the map,
* detect faces,
* recognize faces,
* approach faces and use synthesized speech to "talk" to faces,
* detect rings of color: black, blue, red, green,
* pick up rings (needs a special hook attached; uses camera information for targeting),
* combine the listed tasks into a high level task: 
	* explore the map,
	* find people,
	* bring each person the desired ring.
