#include <ros/ros.h>
#include <tf/transform_listener.h>

int main(int argc, char** argv){
  ros::init(argc, argv, "drop1");

  ros::NodeHandle node;

  tf::TransformListener listener;

  ros::Rate rate(10.0);
  while (node.ok()){
    tf::StampedTransform transform;
    try{
      //listener.lookupTransform("/turtle2", "/turtle1",
      //                         ros::Time(0), transform);
      listener.lookupTransform("/base_link", "/map",
                               ros::Time(0), transform);
    }
    /* 
    try{
        ros::Time now = ros::Time::now(); //we can change this time to anything
        ros::Time delay_5 = ros::Time::now() - ros::Duration(5.0); //we can change this time to anything
        listener.waitForTransform(  "/turtle2", now,
                                    "/turtle1", delay_5,
                                    "/world",
                                    ros::Duration(3.0)); //(_,_,time,how long do I wait for it)
        listener.lookupTransform( "/turtle2", now,  //transform turtle2's now position
                                  "/turtle1", delay_5,  //to turtle1's position 5sec ago
                                  "/world", // name of the frame that is FIXED
                                  transform);
    }
    */
    catch (tf::TransformException &ex) {
      ROS_ERROR("%s",ex.what());
      ros::Duration(1.0).sleep();
      continue;
    }

    /*
    visualization_msgs::Marker marker;
    // Set the frame ID and timestamp.  See the TF tutorials for information on these.
    //marker.header.frame_id = "/base_link";
    marker.header.frame_id = "/map";    //we want map coordinates
    marker.header.stamp = ros::Time::now();

    // Set the namespace and id for this marker.  This serves to create a unique ID
    // Any marker sent with the same namespace and id will overwrite the old one
    marker.ns = "drop_makers";
    marker.id = counter;

    // Set the marker type.  Initially this is CUBE, and cycles between that and SPHERE, ARROW, and CYLINDER
    marker.type = visualization_msgs::Marker::CUBE;

    // Set the marker action.  Options are ADD, DELETE, and new in ROS Indigo: 3 (DELETEALL)
    marker.action = visualization_msgs::Marker::ADD;

    // Set the pose of the marker.  This is a full 6DOF pose relative to the frame/time specified in the header
    marker.pose.position.x = 0;
    marker.pose.position.y = 0;
    marker.pose.position.z = 0;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;

    // Set the scale of the marker -- 1x1x1 here means 1m on a side
    marker.scale.x = 0.2;
    marker.scale.y = 0.2;
    marker.scale.z = 0.2;

    // Set the color -- be sure to set alpha to something non-zero!
    marker.color.r = 0.0f;
    marker.color.g = 1.0f;
    marker.color.b = 0.0f;
    marker.color.a = 0.5; //color alpha

    marker.lifetime = ros::Duration();

    // Publish the marker
    //change this to wait for array or marker
    while (g_array_pub.getNumSubscribers() < 1)
    {
      ROS_WARN_ONCE("Please create a subscriber to the marker");
      sleep(1);
    }

    marker_array.markers.push_back(marker);
    g_marker_pub.publish(marker); 
    g_array_pub.publish(marker_array);  
    */

    rate.sleep();
  }
  return 0;
};