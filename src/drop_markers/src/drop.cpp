#include <cstdlib>
#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <tf/tf.h>

ros::Publisher g_marker_pub;
ros::Publisher g_array_pub;

/* be careful with global variables; declare these two when called
tf::TransformBroadcaster br;
tf::Transform transform;
*/

uint32_t counter = 0;
visualization_msgs::MarkerArray marker_array;

void publishCallback(const ros::TimerEvent& e){

	// tf::Transform transform;
 //    tf::TransformBroadcaster br;
 //    transform.setOrigin( tf::Vector3(0.0, 0.0, 0.0) );
 //    transform.setRotation( tf::Quaternion(0, 0, 0, 1) );
 //    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "base_link", "map"));

	tf::TransformListener listener;
	tf::StampedTransform transform;
  	try{
		//ros::Time now=ros::Time(0);
		listener.waitForTransform("/map","/base_link",ros::Time(0), ros::Duration(5.0));
 		listener.lookupTransform("/map", "/base_link",ros::Time(0), transform);
  	}
  	catch (tf::TransformException ex){
  		ROS_ERROR("%s",ex.what());
	  	ros::Duration(1.0).sleep();
  	}

	visualization_msgs::Marker marker;
    // Set the frame ID and timestamp.  See the TF tutorials for information on these.
    //marker.header.frame_id = "/base_link";
    marker.header.frame_id = "/map";
    marker.header.stamp = ros::Time::now();

    // Set the namespace and id for this marker.  This serves to create a unique ID
    // Any marker sent with the same namespace and id will overwrite the old one
    marker.ns = "drop_makers";
    marker.id = counter;

    // Set the marker type.  Initially this is CUBE, and cycles between that and SPHERE, ARROW, and CYLINDER
    marker.type = visualization_msgs::Marker::CUBE;

    // Set the marker action.  Options are ADD, DELETE, and new in ROS Indigo: 3 (DELETEALL)
    marker.action = visualization_msgs::Marker::ADD;

    // Set the pose of the marker.  This is a full 6DOF pose relative to the frame/time specified in the header
    marker.pose.position.x = transform.getOrigin().x();
    marker.pose.position.y = transform.getOrigin().y();
    marker.pose.position.z = 0;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;

    // Set the scale of the marker -- 1x1x1 here means 1m on a side
    marker.scale.x = 0.2;
    marker.scale.y = 0.2;
    marker.scale.z = 0.2;

    // Set the color -- be sure to set alpha to something non-zero!
    marker.color.r = 0.0f;
    marker.color.g = 1.0f;
    marker.color.b = 0.0f;
    marker.color.a = 0.5;	//color alpha

    marker.lifetime = ros::Duration();

    // Publish the marker
    //change this to wait for array or marker
    while (g_array_pub.getNumSubscribers() < 1)
    {
      ROS_WARN_ONCE("Please create a subscriber to the marker");
      sleep(1);
    }

    
    
    
    marker_array.markers.push_back(marker);
    g_marker_pub.publish(marker);	
   	g_array_pub.publish(marker_array);	
    counter++;
}

int main(int argc, char** argv){
	ros::init(argc, argv, "drop");
	ros::NodeHandle n;

	g_marker_pub = n.advertise<visualization_msgs::Marker> ("visualization_marker", 0);
	g_array_pub = n.advertise<visualization_msgs::MarkerArray> ("visualization_marker_array", 0);

	ros::Timer publish_timer = n.createTimer(ros::Duration(1), publishCallback);

	ros::spin();
}
