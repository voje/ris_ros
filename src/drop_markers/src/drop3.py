#!/usr/bin/env python  
import roslib
import rospy
import math
import tf
import geometry_msgs.msg
from std_msgs.msg import String
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray


def talker():
    MARKERS_MAX = 100
    count = 0
    pub = rospy.Publisher('visualization_marker_array', MarkerArray, queue_size=10)
    listener = tf.TransformListener() 

    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        #get listener
        try:
            (trans,rot) = listener.lookupTransform('/map', '/base_link', rospy.Time(0))
            print(trans)
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue

    marker = Marker()
    marker.header.frame_id = "/map"
    marker.type = marker.SPHERE
    marker.action = marker.ADD
    marker.scale.x = 0.2
    marker.scale.y = 0.2
    marker.scale.z = 0.2
    marker.color.a = 1.0
    marker.pose.orientation.w = 1.0
    marker.pose.position.x = trans[0]
    marker.pose.position.y = trans[1]
    marker.pose.position.z = 0

	if(count > MARKERS_MAX):
		markerArray.markers.pop(0)

	markerArray.markers.append(marker)
	# Renumber the marker IDs
	id = 0
	for m in markerArray.markers:
		m.id = id
	       	id += 1

	pub.publish(markerArray)
	count = count + 1

    rate.sleep()

if __name__ == '__main__':
    rospy.init_node('drop3')
    markerArray = MarkerArray()
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
