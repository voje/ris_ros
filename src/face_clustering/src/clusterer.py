import sys
import math

NUM_FACES_THRESHOLD = 15
DISTANCE_THRESHOLD = 1

class Face:
	def __init__(self, x, y):
		self.x = x
		self.y = y
		self.confirmed = False 

	def dist_to(self, face1):
		return math.sqrt( math.pow(self.x - face1.x, 2) + math.pow(self.y - face1.y, 2) )

	def dist_to_cluster(self, cluster):
		return math.sqrt( math.pow(self.x - cluster.center_x, 2) + math.pow(self.y - cluster.center_y, 2) )

	def to_string(self):
		print "face:\nx: ", self.x, "\ny: ", self.y 

class Cluster:
	#define around at least one face
	def __init__(self, face):
		self.detected = False
		self.visited = False
		self.faces = [face]
		self.center_x = -8000.0
		self.center_y = -8000.0
		self.calc_center()

	def calc_center(self):
		sum_x = 0
		sum_y = 0
		l = len(self.faces)
		for (index, face) in enumerate(self.faces):
			sum_x = sum_x + face.x
			sum_y = sum_y + face.y
		self.center_x = sum_x / float(l)
		self.center_y = sum_y / float(l)

	def add_face(self, face):
		self.faces.append(face)
		self.calc_center()
		if len(self.faces) >= NUM_FACES_THRESHOLD:
			self.detected = True
			print "FACE DETECTED: (%f, %f)" % (face.x, face.y)

	def to_string(self):
		print "cluster:"
		print "num of faces: ", len(self.faces)
		print "center_x: ", self.center_x, ", center_y: ", self.center_y
		for f in self.faces:
			f.to_string()

# keeps track of all possible faces, keeps an array of legit faces and information
# whether they were visited
class Clusterer:
	def __init__(self):
		self.clusters = []
		self.detected = 0
		self.visited = 0

	def insert_face(self, face1):
		min_pair = self.map_face_to_cluster(face1)
		#adding		
		if min_pair[0] == -1:
			#print "new cluster (first)"
			self.clusters.append(Cluster(face1))
		elif min_pair[1] > DISTANCE_THRESHOLD:
			#print "adding new cluster"
			self.clusters.append(Cluster(face1))
		elif min_pair[1] <= DISTANCE_THRESHOLD:
			#print "adding to cluster"
			self.clusters[min_pair[0]].add_face(face1)
		else:
			print "You shouldn't be seeing this."
		# reset the detected variable
		self.detected = 0
		for cl in self.clusters:
			if cl.detected:
				self.detected= self.detected + 1

	def map_face_to_cluster(self, face1):
		#returns (cluster_index, distance), default: (-1, sys.maxint)
		#find closest in clusters 
		min_pair = (-1, sys.maxint) #(index, val)
		for (index, cluster) in enumerate(self.clusters):
			dist = face1.dist_to_cluster(cluster)
			if dist < min_pair[1]:
				min_pair = (index, dist)
		#print "found_match: ", min_pair
		return min_pair

	def face_was_visited(self, face1):
		pair = self.map_face_to_cluster(face1)
		cl_index = pair[0]
		if cl_index == -1 or pair[1] > DISTANCE_THRESHOLD:
			print "Error: visited a vace that wasn't in a cluster."
			return
		if self.clusters[cl_index].detected == False:
			print "Error: visited a face with too few likes."
			return
		if self.clusters[cl_index].visited == True:
			print "Error: stuff's broken. Check line 104."
			return
		self.clusters[cl_index].visited = True
		self.visited = self.visited + 1

	def visited_detected(self):
		return (self.visited, self.detected)

	def can_i_visit_this(self, face1):
		#face must be not visited and detected
		pair = self.map_face_to_cluster(face1)
		cl_index = pair[0]
		if cl_index == -1:
			return False
		if pair[1] > DISTANCE_THRESHOLD:
			return False
		if (not self.clusters[cl_index].visited) and self.clusters[cl_index].detected:
			return True
		return False

	def to_string(self):
		l = len(self.clusters)
		print "num clusters: ", l, "detected: ", self.detected, ", visited: ", self.visited
		for c in self.clusters:
			c.to_string()

	def to_string1(self):
		for cl in self.clusters:
			print "[", len(cl.faces), "v:", cl.visited, "d", cl.detected, "], ",	



if __name__ == "__main__":
	f1 = Face(1, 1)
	f2 = Face(1, 2)
	f3 = Face(2, 1)
	f4 = Face(8, 8)
	f5 = Face(9, 9)
	f6 = Face(8, 9)
	f7 = Face(3, 1)
	f8 = Face(8, 8)
	f9 = Face(22, 2)
	f10 = Face(22, 4)
	f11 = Face(21, 3)

	cl = Clusterer()

	fff = [f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11]
	for f in fff:
		cl.insert_face(f)

	test_face = Face(22, 3)
	cl.can_i_visit_this(test_face)
	cl.face_was_visited(test_face)
	cl.can_i_visit_this(test_face)

	cl.to_string1()


