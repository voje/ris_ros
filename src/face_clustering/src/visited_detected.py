#!/usr/bin/env python
import rospy
from face_clustering.msg import visited_detected

visited=0
detected=0

def visited_detected():
    pub = rospy.Publisher('visited_detected', visited_detected)
    rospy.init_node('visited_detected_node', anonymous=True)
    r = rospy.Rate(10) #10hz
    msg = visited_detected()
    msg.visited = visited
    msg.detected = detected

    while not rospy.is_shutdown():
        rospy.loginfo(msg)
        pub.publish(msg)
        r.sleep()

if __name__ == '__main__':
    try:
        visited_detected()
    except rospy.ROSInterruptException: pass