#!/usr/bin/env python
import rospy
from face_clustering.srv import *
from face_clustering.msg import visited_detected
from clusterer import *
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
from pos_transformer.srv import *

def visited_detected_pub():
    pub = rospy.Publisher("visited_detected", visited_detected, queue_size=10)
    rate = rospy.Rate(10) # 10hz
    print "Publishing to visited_detected."
    while not rospy.is_shutdown():
        vd = cl.visited_detected()
        pub.publish(vd[0], vd[1])
        rate.sleep()

def callback_visiting(req):
    face = Face(req.x, req.y) 
    boo = cl.can_i_visit_this(face)
    numeric = 0
    if boo:
        numeric = 1
    return visiting_faceResponse(numeric)

def callback_reached(req):
    cl.face_was_visited( Face(req.x, req.y) )    
    return []

def callback_markers(data):

    rospy.wait_for_service("cam_to_map")

    for marker in data.markers:
        transformed_mark = None
        try:
            ctm = rospy.ServiceProxy("cam_to_map", vis_msg_mark)
            resp1 = ctm(marker)
            transformed_mark = resp1.out_marker
        except rospy.ServiceException, e:
            #print "cam_to_map service call failed: %s" % e
            pass

        if transformed_mark != None:
            x_cam = marker.pose.position.x
            y_cam = marker.pose.position.y
            x_map = transformed_mark.pose.position.x
            y_map = transformed_mark.pose.position.y

            #print "x_cam: %f, y_cam: %f, x_map: %f, y_map: %f" % (x_cam, y_cam, x_map, y_map)
            #print "z: %f" % marker.pose.position.z

            f = Face(x_map, y_map)
            cl.insert_face(f)
    
def server():
    rospy.init_node('face_server')

    s=rospy.Service('fs_visiting_face', visiting_face, callback_visiting)
    print "Server fs_visiting_face pripravljen."

    s=rospy.Service('fs_reached', reached, callback_reached)
    print "Server fs_reached pripravljen."

    sub=rospy.Subscriber("/facemapper/markers",MarkerArray, callback_markers)

    try:
        visited_detected_pub()
    except rospy.ROSInterruptException:
        pass

    #? rospy.spin()

if __name__ =="__main__":
    cl = Clusterer()

    #testing#######
    """
    f1 = Face(1, 1)
    f2 = Face(1, 2)
    f3 = Face(2, 1)

    f4 = Face(8, 8)
    f5 = Face(9, 9)
    f6 = Face(8, 9)
    f7 = Face(9, 8)
    f8 = Face(9, 8)

    f9 = Face(22, 22)
    f10 = Face(22, 23)
    f11 = Face(21, 22)

    fff = [f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11]

    for f in fff:
        cl.insert_face(f)
    """
    ###############

    #DON'T FORGET ME!!!
    server()


