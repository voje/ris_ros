#!/usr/bin/env python
# license removed for brevity
import roslib
import rospy
import actionlib
from std_msgs.msg import String
from geometry_msgs.msg import PoseStamped
from visualization_msgs.msg import Marker
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import Pose, PoseWithCovarianceStamped, Point, Quaternion, Twist
import tf

def talker():
    tar_pose = Pose(Point(-0.989, 1.221, 0), Quaternion(0,0,0,1.0))

    move_base = actionlib.SimpleActionClient("move_base", MoveBaseAction) 
    goal = MoveBaseGoal()

    while not rospy.is_shutdown():
        goal.target_pose.pose = tar_pose
        goal.target_pose.header.frame_id = "map"
        goal.target_pose.header.stamp = rospy.Time.now()

        move_base.send_goal(goal)
        state = move_base.get_state()
        print state

    #pub = rospy.Publisher("/move_base_simple/goal", PoseStamped, queue_size=10)
    #rospy.init_node("traversal", anonymous=True)

    #rate = rospy.Rate(10) # 10hz

    while not rospy.is_shutdown():
    	pose = PoseStamped()
    	pose.header.stamp = rospy.Time.now()
    	pose.header.frame_id = "map"
    	pose.pose.position = Point(-0.989, 1.221, 0)
    	pose.pose.orientation = Quaternion(0,0,0,1.0)
        pub.publish(pose)
        #rate.sleep()
        print move_base.get_state() 


if __name__ == '__main__':
    #rospy.init_node("traversal") 
    try:
    	talker()

    except rospy.ROSInterruptException:
    	print "program interrupted before completion"


#todo
# 1. map of points - loop through the points
# 2. transpose point from map to base_link
# 3. send point as goal (publish to above topic)
# 4. wait for robot to reach goal
# 5. goto 1.




