#!/usr/bin/env python
# license removed for brevity
import roslib
import rospy
import actionlib
from std_msgs.msg import String
from geometry_msgs.msg import PoseStamped
from visualization_msgs.msg import Marker
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import Pose, PoseWithCovarianceStamped, Point, Quaternion, Twist
import tf

"""
def mark_goal(goal):
	listener = tf.transformListener()
	while not rospy.is_shutdown():
		try:
			(trans,rot) = listener.lookupTransform('/map', '/base_link', rospy.Time(0))
			print(trans)
		except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
			continue
	# visualize the next location	
    marker = Marker()
    marker.header.frame_id = "/map"
    marker.type = marker.SPHERE
    marker.action = marker.ADD
    marker.scale.x = 0.2
    marker.scale.y = 0.2
    marker.scale.z = 0.2
    marker.color.a = 1.0
    marker.pose.orientation.w = 1.0
    marker.pose.position.x = goal.pose.x
    marker.pose.position.y = goal.pose.y
    marker.pose.position.z = 0

    marker_pub = rospy.Publisher('visualization_marker', Marker, queue_size=10)
    marker_pub.publish(marker)
"""

def talker():
    pub = rospy.Publisher("/move_base_simple/goal", PoseStamped, queue_size=10)
    rospy.init_node("traversal", anonymous=True)
    #rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
    	pose = PoseStamped()
    	pose.header.stamp = rospy.Time.now();
    	pose.header.frame_id = "map"
	pose.pose.position = Point(-0.989, 1.221, 0)
	pose.pose.orientation = Quaternion(0,0,0,1.0)
        pub.publish(pose)
        #rate.sleep()

if __name__ == '__main__':
    try:
    	#rospy.init_node("traversal") 
    	#traversal_client()
    	#traversal_client1()
    	talker()

    except rospy.ROSInterruptException:
    	print "program interrupted before completion"


# topic and message:
"""
rostopic pub /move_base_simple/goal geometry_msgs/PoseStamped 
'{ 
header: 
	{stamp: now, frame_id: "map"}, 
pose: 
	{ 
	position: 
		{x: -4.560, y: 0.204, z: 0.0}, 
	orientation: 
		{w: 1.0}
	}
}'
"""

#todo
# 1. map of points - loop through the points
# 2. transpose point from map to base_link
# 3. send point as goal (publish to above topic)
# 4. wait for robot to reach goal
# 5. goto 1.




