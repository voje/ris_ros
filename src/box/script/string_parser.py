#!/usr/bin/env python
import rospy
from box_messages.srv import *

def send(str_face, str_dest):
	rospy.wait_for_service("goal_receiver")	
	try:
		goal_receiver = rospy.ServiceProxy("goal_receiver", Generic)
		resp1 = goal_receiver(str_face, str_dest, 0, 0)
		return resp1.int3
	except rospy.ServiceException, e:
		print "Service call failed: %s" % e

if __name__ == "__main__":
	rospy.init_node("string_parser")
	# string = parse_string()
	string = ("Prevc", "Hotel")
	print send(string[0], string[1])

