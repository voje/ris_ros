#!/usr/bin/env python
import rospy
import tf
import math
from visualization_msgs.msg import Marker, MarkerArray
from geometry_msgs.msg import PoseStamped
from clusterer import *

COUNTER_THRESHOLD = 5		#cluster becomes discovered when it reaches 20 objects
DISTANCE_THRESHOLD = 0.3	#in meters?
FACE_FILTER_RANGE = [0.3, 0.5]

faces_cl = Clusterer("faces_cl", COUNTER_THRESHOLD, DISTANCE_THRESHOLD)
all_clusterers = [faces_cl]

def transform_cam_to_map(marker):
	global tf_lis
	old_pose = PoseStamped()
	old_pose.header = marker.header
	old_pose.pose = marker.pose
	tf_lis.waitForTransform("/camera_rgb_optical_frame", "/map", rospy.Time(), rospy.Duration(1.0))
	new_pose = tf_lis.transformPose("/map", old_pose)
	marker.header = new_pose.header
	marker.pose = new_pose.pose
	return marker

def pass_face_filter(marker):
	z = marker.pose.position.z
	if z >= FACE_FILTER_RANGE[0] and z <= FACE_FILTER_RANGE[1]:
		return True
	return False

def faces_callback(data):
	# insert newly detected objects into clusters
	for marker in data.markers:
		try:
			marker = transform_cam_to_map(marker)
			if pass_face_filter(marker):
				newobj = Obj(marker)
				faces_cl.insert_obj(newobj)
		except:
			pass

def create_marker_array(selected_clusterers):
	marr = MarkerArray()
	for (i,clusterer) in enumerate(selected_clusterers):
		for (j,cluster) in enumerate(clusterer.clusters):
			if cluster.detected:
				mar = Marker()
				mar.id = (i+1)*(j+1)
				mar.header.frame_id = "map"
				mar.header.stamp = rospy.Time.now()
				mar.type = Marker.SPHERE
				#mar.action = Marker.ADD
				mar.pose.position.x = cluster.center_x
				mar.pose.position.y = cluster.center_y
				mar.pose.position.z = cluster.center_z
				mar.pose.orientation.x = 0
				mar.pose.orientation.y = 0
				mar.pose.orientation.z = 0
				mar.pose.orientation.w = 1
				mar.scale.x = 0.2
				mar.scale.y = 0.2
				mar.scale.z = 0.2 
				mar.color.a = 1.0
				mar.color.r = 0.0
				mar.color.g = 0.0
				mar.color.b = 1.0

				marr.markers.append(mar)
	#selected_clusterers[0].to_string2()
	return marr

def dist2d(x1, y1, x2, y2):
	return math.sqrt( math.pow(x1-x2, 2) + math.pow(y1-y2, 2) )

def scanner_callback(req):
	marr = MarkerArray()
	#get self pose first
	rad = req.radius
	#add more clusterers here...
	#go through all
	for (i,clusterer) in enumerate(all_clusterers):
		for (j,cluster) in enumerate(clusterer):
			if dist2d(pose, cluster) <= rad:
				mar = Marker()
				mar.id = (i+1)*(j+1)
				mar.header.frame_id = "map"
				mar.header.stamp = rospy.Time.now()
				mar.type = Marker.SPHERE
				#mar.action = Marker.ADD
				mar.pose.position.x = cluster.center_x
				mar.pose.position.y = cluster.center_y
				mar.pose.position.z = cluster.center_z
				mar.pose.orientation.x = 0
				mar.pose.orientation.y = 0
				mar.pose.orientation.z = 0
				mar.pose.orientation.w = 1
				mar.scale.x = 0.2
				mar.scale.y = 0.2
				mar.scale.z = 0.2 
				mar.color.a = 1.0
				mar.color.r = 0.0
				mar.color.g = 1.0
				mar.color.b = 0.0
				marr.markers.append(mar)
	return marr

if __name__ == "__main__":
	#listen on different topics
	#filter
	#add to clusters
	#publish full clusters as markers in a marker array

	rospy.init_node("object_mapper")
	rospy.Service("surround_obj_scanner", Surround, scanner_callback)
	tf_lis = tf.TransformListener()

	rate = rospy.Rate(10)
	while not rospy.is_shutdown():

		rospy.Subscriber("facemapper/markers", MarkerArray, faces_callback)

		#publish all clusters in a markerArray
		pub = rospy.Publisher("mapped_objects", MarkerArray, queue_size=10)
		#go through clusters, add faces, signs, objects with appropriate ids
		marr = create_marker_array(all_clusterers)
		pub.publish(marr)

		rate.sleep()
