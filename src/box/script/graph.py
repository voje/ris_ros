#!/usr/bin/env python
from visualization_msgs.msg import Marker

def random_nodes(boundaries):
	# todo (extra points)
	# need to see map for this one	
	print "todo"

class Node:
	def __init__(self, node_id, marker=None):
		self.id = node_id
		self.vertices = []
		self.marker = Marker()
		if marker is not None:
			self.marker = marker

	def toString(self):
		v = ""
		for ver in self.vertices:
			v += "(%s, %s), " % (ver[0], ver[1])
		s = "%s (%f, %f, %f), [%s]\n" % (self.id, self.marker.pose.position.x, self.marker.pose.position.y, self.marker.pose.position.z, v)
		return s

class Graph:
	def __init__(self, random=False):
		if random:
			self.nodes = random_nodes(boundaries)	
		else:
			self.nodes = {}

	def add_node(self, newnode):
		for key, node in self.nodes.iteritems():
			if key == newnode.id:
				print "Node with id='%s' already exists." % (newnode.id)
				return
		self.nodes[newnode.id] = newnode

	def add_vertex(self, vertex):
		# for now: two-way graph (add vertex to both nodes)
		# vertex = (out_id, in_id)
		#check
		out_node = self.nodes[vertex[0]]
		for v in out_node.vertices:
			if v[1] == vertex[1]:
				print "Vertex (%s, %s) already exists." % (vertex[0], vertex[1])
				return	
		in_node = self.nodes[vertex[1]]
		#double check. If the first loop succeeded, this one should too
		swap_vertex = (vertex[1], vertex[0])
		for v in in_node.vertices:
			if v[1] == swap_vertex[1]:
				print "Something went wrong when adding vertex (%s, %s)." % (vertex[0], vertex[1])
				return	
		#add
		out_node.vertices += [vertex]
		in_node.vertices += [swap_vertex]

	def remove_vertex(self, vertex):
		for k, n in self.nodes.iteritems():
			for i, v in enumerate(n.vertices):
				if v == vertex:
					n.vertices.pop(i)

	def remove_node(self, node_id):
		for k, n in self.nodes.iteritems():
			if k == node_id:
				if len(n.vertices) != 0:
					print "Cann0t remove a linked node: %s" % n.toString()
					return 
		self.nodes.pop(node_id, None)

	def toString(self):
		s = ""
		for key, n in self.nodes.iteritems():
			tmp = n.toString()
			s += tmp
		return s

	def add_simple_walk(self):
		keys = self.nodes.keys()
		for i in range(0, len(keys)-1):
			self.add_vertex((keys[i], keys[i+1]))
		self.add_vertex((keys[-1], keys[0]))


if __name__ == "__main__":
	nodes = [
		Node("n1"),
		Node("n2"),
		Node("n3"),
		Node("n4"),
		Node("n5"),
		Node("n6")
	]

	vertices = [
		("n2", "n3"),
		("n2", "n5"),
		("n3", "n4"),
		("n3", "n5"),
		("n5", "n3")
	]

	g = Graph()
	for n in nodes:
		g.add_node(n)

	for v in vertices:
		g.add_vertex(v)

	g.remove_node("n1")

	print g.toString()