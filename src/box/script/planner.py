#!/usr/bin/env python
import rospy
import rosbag
from box_messages.srv import *
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
from graph import *

bag_filename = "/home/kristjan/FRI/ROS/ris_ros/bags/explorer_walk_test2.bag"

graph = Graph()
node_ids_idx = -1 #this is our current position on the graph
current_idx = -1
current_path = [0,1,2,3,4,5,6]

def get_node_by_id(n_id):
	global graph
	keys = graph.nodes.keys()
	for key in keys:
		if key == n_id:
			return graph.nodes[n_id]
	return None

def fill_from_file(bag_filename):
	bag = rosbag.Bag(bag_filename)
	for topic, msg, t in bag.read_messages(topics=['explorer_walk_markers']):
	    marr = msg
	    for marker in marr.markers:
	    	graph.add_node(Node(marker.id, marker))
	    #todo
	bag.close()
	print "Reading from %s complete." % bag_filename

def planner_callback(req):
	global node_ids_idx #obsolete
	global current_idx
	node_ids_idx += 1 #obsolete
	current_idx += 1
	#break condition (in this case end of simple walk)
	if current_idx >= len(current_path):
		print ""
		mar = Marker()
		mar.ns = "out_of_nodes"
		print "Requested path plan to %s. -> out of nodes" % (req.str1)
	#normal condition
	else:
		nod = get_node_by_id(current_path[current_idx])
		mar = nod.marker
		print "Requested path plan to %s. -> Sending graph.nodes[%d]." % (req.str1, nod.id)
	return AskPPResponse(mar)

if __name__ == "__main__":
	rospy.init_node("planner_node")

	fill_from_file(bag_filename)
	graph.add_simple_walk() #connects the nodes (vertices)

	#tracking our position in map
	node_ids = graph.nodes.keys()

	print graph.toString()

	rospy.Service("path_planner", AskPP, planner_callback)
	rospy.spin()


