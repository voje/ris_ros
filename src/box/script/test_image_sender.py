#!/usr/bin/env python
import rospy
from sensor_msgs.msg import Image
import cv_bridge
import cv2

def talker():
    pub = rospy.Publisher('test_images', Image, queue_size=10)
    rospy.init_node('talker', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    img = cv2.imread("yaleB02_4.pgm", 1)
    cvb = cv_bridge.CvBridge()
    while not rospy.is_shutdown():
        ros_img = Image()
        ros_img = cvb.cv2_to_imgmsg(img, "bgr8")
        pub.publish(ros_img)
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass