#!/usr/bin/env python
import rospy
from visualization_msgs.msg import Marker
import tf
import math

def create_marker():
	mar = Marker()
	mar.header.frame_id = "map"
	mar.header.stamp = rospy.Time.now()
	mar.type = Marker.ARROW
	mar.pose.position.x = 0
	mar.pose.position.y = 0
	mar.pose.position.z = 0
	quaternion = tf.transformations.quaternion_from_euler(0, 0, 3*math.pi/2) # 0 radians is SOUTH
	mar.pose.orientation.x = quaternion[0]
	mar.pose.orientation.y = quaternion[1]
	mar.pose.orientation.z = quaternion[2]
	mar.pose.orientation.w = quaternion[3]
	mar.scale.x = 0.4
	mar.scale.y = 0.1
	mar.scale.z = 0.1
	mar.color.a = 1.0
	mar.color.r = 1.0
	mar.color.g = 1.0
	mar.color.b = 1.0
	return mar

if __name__ == "__main__":
	pub = rospy.Publisher('test_marker', Marker, queue_size=10)
	rospy.init_node('send_test_marker')
	r = rospy.Rate(10) # 10hz
	while not rospy.is_shutdown():
		mar = create_marker()
		pub.publish(mar)
		r.sleep()