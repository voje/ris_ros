#!/usr/bin/env python
import rospy
import rosbag
import sys
from geometry_msgs.msg import Pose
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray

marr = MarkerArray()

def pose_callback(data):
	mar = Marker()
	mar.ns = "explorer"
	mar.id = len(marr.markers)
	mar.header.frame_id = "/map"
	mar.header.stamp = rospy.Time.now()
	mar.type = Marker.ARROW
	mar.pose.position = data.position
	mar.pose.orientation = data.orientation
	mar.scale.x = 0.5
	mar.scale.y = 0.2
	mar.scale.z = 0.2
	mar.color.a = 1.0
	mar.color.r = 1.0
	mar.color.g = 0.0
	mar.color.b = 0.0

	marr.markers.append(mar)
	print "Marker %d added." % (mar.id)
	sub_once.unregister() #read only once

if __name__ == "__main__":
	rospy.init_node("create_path")
	pub = rospy.Publisher("path_creation_marker_array", MarkerArray, queue_size=10)
	global sub_once
	while(True):
		print "looping"
		# publish created markers
		pub.publish(marr)

		# do stuff
		key = input("1: quit, 2: save marker")
		if key == 1:
			break
		else:
			print "Waiting for robot_pose."
			rospy.wait_for_message("robot_pose", Pose, timeout=7)
			print "Robot_pose message received."
			sub_once = rospy.Subscriber("robot_pose", Pose, pose_callback)

	#save to bag file
	bag = rosbag.Bag('explorer_walk_markers.bag', 'w')
	try:
	    bag.write('explorer_walk_markers', marr)
	finally:
	    bag.close()


