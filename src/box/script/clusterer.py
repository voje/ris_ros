import rospy
import sys
import math
from visualization_msgs.msg import Marker

class Obj:
	def __init__(self, marker):
		self.marker = marker

	def dist_to(self, obj1):
		return math.sqrt( math.pow(self.marker.pose.position.x - obj1.marker.pose.position.x, 2) + math.pow(self.marker.pose.position.y - obj1.marker.pose.position.y, 2) )

	def dist_to_cluster(self, cluster):
		return math.sqrt( math.pow(self.marker.pose.position.x - cluster.center_x, 2) + math.pow(self.marker.pose.position.y - cluster.center_y, 2) )

	def to_string(self):
		print "object: ", self.marker.id ,"\nx: ", self.marker.pose.position.x, "\ny: ", self.marker.pose.position.y 

class Cluster:
	#define around at least one object
	def __init__(self, obj, cl_id, counter_threshold, distance_threshold):
		self.id = cl_id
		self.distance_threshold = distance_threshold
		self.counter_threshold = counter_threshold
		self.objects = [obj]
		self.center_x = -8000.0
		self.center_y = -8000.0
		self.center_z = -8000.0
		self.calc_center()
		self.detected = False

	def calc_center(self):
		sum_x = 0
		sum_y = 0
		sum_z = 0
		l = len(self.objects)
		for (index, obj) in enumerate(self.objects):
			sum_x = sum_x + obj.marker.pose.position.x
			sum_y = sum_y + obj.marker.pose.position.y
			sum_z = sum_z + obj.marker.pose.position.z
		self.center_x = sum_x / float(l)
		self.center_y = sum_y / float(l)
		self.center_z = sum_z / float(l)

	def add_obj(self, newobj):
		if len(self.objects) >= self.counter_threshold:
			return
		self.objects.append(newobj)
		self.calc_center()
		if len(self.objects) >= self.counter_threshold:
			if not self.detected:
				print "clusterer: object detected: %s." % (self.id)
			self.detected = True

	def to_string(self):
		print "cluster_id: %s, num_of_obj: %d" % (self.id, len(self.objects))
		print "center_xyz: (%f, %f, %f)" % (self.center_x, self.center_y, self.center_z)
		for f in self.objects:
			f.to_string()

class Clusterer:
	def __init__(self, cl_id, counter_threshold, distance_threshold):
		self.id = cl_id
		self.counter_threshold = counter_threshold #how many objects for a cluster to become discovered
		self.distance_threshold = distance_threshold #how far from cluster center, to still include an obj
		self.clusters = []
		self.detected = 0

	def insert_obj(self, newobj):
		#at insertion, we check distance to similar objects, later we will insert based on recognition
		min_pair = self.map_obj_to_cluster(newobj)
		#adding		
		if min_pair[0] == -1:
			#print "new cluster (first)"
			self.clusters.append(Cluster(newobj, newobj.marker.id, self.counter_threshold, self.distance_threshold))
		elif min_pair[1] > self.distance_threshold:
			#print "adding new cluster"
			self.clusters.append(Cluster(newobj, newobj.marker.id, self.counter_threshold, self.distance_threshold))
		elif min_pair[1] <= self.distance_threshold:
			#print "adding to cluster"
			self.clusters[min_pair[0]].add_obj(newobj)
		else:
			print "You shouldn't be seeing this."
		# reset the detected variable
		self.detected = 0
		for cl in self.clusters:
			if cl.detected:
				self.detected= self.detected + 1

	def map_obj_to_cluster(self, newobj):
		#returns (cluster_index, distance), default: (-1, sys.maxint)
		#find closest in clusters 
		min_pair = (-1, sys.maxint) #(index, val)
		for (index, cluster) in enumerate(self.clusters):
			dist = newobj.dist_to_cluster(cluster)
			if dist < min_pair[1]:
				min_pair = (index, dist)
		#print "found_match: ", min_pair
		return min_pair

	def to_string(self):
		l = len(self.clusters)
		print "num clusters: ", l, "detected: ", self.detected
		for c in self.clusters:
			c.to_string()

	def to_string1(self):
		for cl in self.clusters:
			print "[", len(cl.objects), "d", cl.detected, "], ",	

	def to_string2(self):
		print "det: %d, num_of_clusters: %d" % (self.detected, len(self.clusters))


def generate_marker(x, y, m_id):
	mar = Marker()
	mar.id = m_id
	mar.header.frame_id = "map"
	mar.header.stamp = None
	mar.type = Marker.CUBE
	mar.pose.position.x = x
	mar.pose.position.y = y
	mar.pose.position.z = 0
	mar.pose.orientation.x = 0
	mar.pose.orientation.y = 0
	mar.pose.orientation.z = 0
	mar.pose.orientation.w = 1
	mar.scale.x = 0.2
	mar.scale.y = 0.2
	mar.scale.z = 1
	mar.color.a = 1.0
	mar.color.r = 0.0
	mar.color.g = 0.0
	mar.color.b = 1.0
	return mar

if __name__ == "__main__":
	f1 = (1, 1, "t")
	f2 = (1, 2, "t")
	f3 = (2, 1, "t")
	f4 = (8, 8, "d")
	f5 = (9, 9, "d")
	f6 = (8, 9, "d")
	f7 = (3, 1, "e")
	f8 = (8, 8, "d")
	f9 = (22, 2, "f")
	f10 = (22, 4, "f")
	f11 = (21, 3, "f")

	cl = Clusterer("faces")

	fff = [f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11]
	for f in fff:
		m = generate_marker(f[0], f[1], f[2])
		obj = Obj(m)
		cl.insert_obj(obj)

	cl.to_string()
	cl.to_string1()


