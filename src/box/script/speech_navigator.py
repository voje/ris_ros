#!/usr/bin/env python
import rospy
import sys
from std_msgs.msg import String

names=["forest", "harry", "scarlet" , "tina" , "peter", "kim", "filip" , "matthew" , "ellen"]
streets=["blue street", "red street" , "green street", "yellow street"]
buildings=["blue building", "red building" , "green building", "yellow building"]

def levenshtein(s1, s2):
    if len(s1) < len(s2):
        return levenshtein(s2, s1)

    # len(s1) >= len(s2)
    if len(s2) == 0:
        return len(s1)

    previous_row = range(len(s2) + 1)
    for i, c1 in enumerate(s1):
        current_row = [i + 1]
        for j, c2 in enumerate(s2):
            insertions = previous_row[j + 1] + 1 # j+1 instead of j since previous_row and current_row are one character longer
            deletions = current_row[j] + 1       # than s2
            substitutions = previous_row[j] + (c1 != c2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row
    
    return previous_row[-1]

def callback(data):
    #rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.data)
    rospy.loginfo(levenshtein("pick up peter on blue street", data.data.lower()))
    bestName, bestStreet, bestLev = None, None, sys.maxint
    for name in names:
        for street in streets:
            lev = levenshtein(name + " is waiting for you on " + street, data.data.lower())
            if (lev < bestLev):
                bestName, bestStreet, bestLev = name, street, lev
    
    rospy.loginfo("%s %s", bestName, bestStreet)

def listener():
    rospy.init_node('speech_listener', anonymous=True)

    rospy.Subscriber("command", String, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    listener()
