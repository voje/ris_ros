#!/usr/bin/env python
import rospy
import numpy as np
import math
import tf
import actionlib
from box_messages.srv import *
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from visualization_msgs.msg import Marker, MarkerArray

global_goals = []
next_global_goal = None
mini_goal = None

def handle_goal_request(req):
	#print "(%s, %s)" % (req.str1, req.str2)
	#mind the order!!!
	global_goals.append(req.str2)	
	global_goals.append(req.str1)
	resp = GenericResponse()
	resp.int3 = 42
	return resp

def listen_for_goals():
	print "Listening for goals."
	serv = rospy.Service("goal_receiver", Generic, handle_goal_request)	
	n_goals = len(global_goals)		
	while len(global_goals) == n_goals and not rospy.is_shutdown():
		pass
	serv.shutdown("Don't want any goals right now.")
	return True

def get_next_mini_goal(dest_name):
	#get marker from planner
	rospy.wait_for_service("path_planner")
	try:
		path_planner = rospy.ServiceProxy("path_planner", AskPP)
		resp = path_planner(dest_name)
	except rospy.ServiceException as exc:
		print("Service did not process request: " + str(exc))

	#handle last goal
	if resp.marker1.ns == "out_of_nodes":
		return None

	goal = MoveBaseGoal()
	goal.target_pose.header = resp.marker1.header
	goal.target_pose.pose = resp.marker1.pose
	return goal

def approach_marker(marker):
	dist = 0.5
	#assuming we are on position of current mini_goal
	global mini_goal
	if mini_goal is None:
		print "I don't know where I am."
		return None
	vec = [0.0, 0.0] #vector, pointing from marker to me (x, y)
	vec[0] = mini_goal.target_pose.pose.position.x - marker.pose.position.x
	vec[1] = mini_goal.target_pose.pose.position.y - marker.pose.position.y
	vec_len = math.sqrt( vec[0]*vec[0] + vec[1]*vec[1] )
	vec_norm = vec
	vec_norm[0] = vec_norm[0] / (vec_len+1e-9)
	vec_norm[1] = vec_norm[1] / (vec_len+1e-9)
	#calculate point, n units away from marker, along vec
	point = [0.0, 0.0]
	point[0] = marker.pose.position.x + dist*vec_norm[0]
	point[1] = marker.pose.position.y + dist*vec_norm[1]
	#calculate the angle
	euler_angle = np.arctan( vec[1] / (vec[0]+1e-9) ) #avoid division by zero
	quaternion = tf.transformations.quaternion_from_euler(0, 0, euler_angle)
	#create marker
	appr_marker = Marker()
	appr_marker.ns = "arrow"
	appr_marker.id = 0
	appr_marker.header.frame_id = "/map"
	appr_marker.header.stamp = rospy.Time.now()
	appr_marker.type = Marker.ARROW
	appr_marker.pose.position.x = point[0]
	appr_marker.pose.position.y = point[1]
	appr_marker.pose.position.z = marker.pose.position.z
	appr_marker.pose.orientation.x = quaternion[0]
	appr_marker.pose.orientation.y = quaternion[1]
	appr_marker.pose.orientation.z = quaternion[2]
	appr_marker.pose.orientation.w = quaternion[3]
	appr_marker.scale.x = 0.4
	appr_marker.scale.y = 0.1
	appr_marker.scale.z = 0.1
	appr_marker.color.a = 1.0
	appr_marker.color.r = 0.0
	appr_marker.color.g = 0.0
	appr_marker.color.b = 1.0
	#todo: make it a goal and go there
	appr_goal = MoveBaseGoal()
	appr_goal.target_pose.header = appr_marker.header
	appr_goal.target_pose.pose = appr_marker.pose
	print "Approaching target."
	go_there(appr_goal, 20)

	"""
	#testing---
	global mini_goal_mar
	pub = rospy.Publisher("approach_test", MarkerArray, queue_size=10)
	marr = MarkerArray()
	marr.markers.append(mini_goal_mar)
	marr.markers.append(marker)
	marr.markers.append(appr_marker)
	print marr
	try:
		pub.publish(marr)
	except:
		print "fail"
	#testing--^
	"""

def go_there(mini_goal, retries=-1):
	global client	
	res = False	
	client.send_goal(mini_goal)
	res = client.wait_for_result(rospy.Duration.from_sec(5.0))
	if retries <= 0:
		while not res:
			print "Failed to reach mini_goal... retrying."
			client.send_goal(mini_goal)
			res = client.wait_for_result(rospy.Duration.from_sec(5.0))
	else:
		for i in range(0, retries):
			print "Failed to reach mini_goal... retrying."
			client.send_goal(mini_goal)
			res = client.wait_for_result(rospy.Duration.from_sec(5.0))
	if res:
		print "Mini_goal reached."
	else:
		print "Failed to reach mini_goal... aborting."

def spin_around():
	#todo!!! study quaternions... or just go for a walk and use geometry_msgs instead
	print "spinning"
	sp = MoveBaseGoal()
	sp.target_pose.header.frame_id="/map"
	sp.target_pose.pose.orientation.x = 0
	sp.target_pose.pose.orientation.y = 0
	sp.target_pose.pose.orientation.z = 1
	sp.target_pose.pose.orientation.w = 1
	go_there(sp)

def surroundings(radius):
	rospy.wait_for_service("surround_obj_scanner")
	try:
		surr = rospy.ServiceProxy("surround_obj_scanner", Surround)
		resp = surr(radius)
	except rospy.ServiceException as exc:
		print("Service did not process request: " + str(exc))
	print resp.objects	#should print out a MarkerArray
	return resp.objects

if __name__ == "__main__":
	rospy.init_node("driver")
	client = actionlib.SimpleActionClient("move_base", MoveBaseAction)	

	"""
	#testing---
	mini_goal_mar = Marker()
	mini_goal_mar.ns = "mini_goal"
	mini_goal_mar.id = 0
	mini_goal_mar.header.frame_id = "/map"
	mini_goal_mar.header.stamp = rospy.Time.now()
	mini_goal_mar.type = Marker.CUBE
	mini_goal_mar.pose.position.x = 2
	mini_goal_mar.pose.position.y = -1
	mini_goal_mar.pose.position.z = 0
	mini_goal_mar.pose.orientation.x = 0
	mini_goal_mar.pose.orientation.y = 0
	mini_goal_mar.pose.orientation.z = 0
	mini_goal_mar.pose.orientation.w = 1
	mini_goal_mar.scale.x = 0.1
	mini_goal_mar.scale.y = 0.1
	mini_goal_mar.scale.z = 0.1
	mini_goal_mar.color.a = 1.0
	mini_goal_mar.color.r = 1.0
	mini_goal_mar.color.g = 0.0
	mini_goal_mar.color.b = 0.0
	mini_goal = MoveBaseGoal()
	mini_goal.target_pose.header = mini_goal_mar.header
	mini_goal.target_pose.pose = mini_goal_mar.pose

	dest_marker = Marker()
	dest_marker.ns = "mini_dest"
	dest_marker.id = 0
	dest_marker.header.frame_id = "/map"
	dest_marker.header.stamp = rospy.Time.now()
	dest_marker.type = Marker.CUBE
	dest_marker.pose.position.x = 2.3
	dest_marker.pose.position.y = -1.4
	dest_marker.pose.position.z = 0
	dest_marker.pose.orientation.x = 0
	dest_marker.pose.orientation.y = 0
	dest_marker.pose.orientation.z = 0
	dest_marker.pose.orientation.w = 1
	dest_marker.scale.x = 0.1
	dest_marker.scale.y = 0.1
	dest_marker.scale.z = 0.1
	dest_marker.color.a = 1.0
	dest_marker.color.r = 0.0
	dest_marker.color.g = 1.0
	dest_marker.color.b = 0.0

	rate = rospy.Rate(10)
	while not rospy.is_shutdown():
		approach_marker(dest_marker)
		rate.sleep()
	#testing--^
	"""

	listen_for_goals()
	print "Current global_goals:"
	print global_goals
	next_global_goal = global_goals.pop()

	print "Pursuing goal: %s." % (next_global_goal)
	while not rospy.is_shutdown():
		client.wait_for_server()

		mini_goal = get_next_mini_goal(next_global_goal)
		#test print
		#print mini_goal
		if mini_goal is None:
			print "Global goal '%s' reached." % (next_global_goal)
			break

		go_there(mini_goal)

	print "Going to sleep now."
