#include <iostream>
#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include "box_messages/Generic.h"

using namespace std;

typedef visualization_msgs::Marker Marker;
typedef visualization_msgs::MarkerArray MarkerArray;
typedef box_messages::Generic Generic;

string face_goal = "";
string dest_goal = "";

bool start_moving(){
	cout << "start_moving" << endl;
}

bool goal_receiver_callback(Generic::Request &req, Generic::Response &res){
	cout << req.str1 << ", " << req.str2 << endl;	
	face_goal = req.str1;
	dest_goal = req.str2;	
	res.int3 = 42;
	start_moving();
	return true;
}

int main(int argc, char** argv){

	Marker testMarker();
	MarkerArray testMarkerArray();	
	Generic testGeneric();

	ros::init(argc, argv, "driver");
	ros::NodeHandle nh;

	ros::ServiceServer string_parser = nh.advertiseService("goal_receiver", goal_receiver_callback);

	ros::spin();

	return 0;
}
