#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/Twist.h>
#include <turtlesim/Spawn.h>

int main(int argc, char** argv){
  ros::init(argc, argv, "my_tf_listener");

  ros::NodeHandle node;

  ros::service::waitForService("spawn");
  ros::ServiceClient add_turtle =
    node.serviceClient<turtlesim::Spawn>("spawn");
  turtlesim::Spawn srv;
  add_turtle.call(srv);

  ros::Publisher turtle_vel =
    node.advertise<geometry_msgs::Twist>("turtle2/cmd_vel", 10);

  tf::TransformListener listener;

  ros::Rate rate(10.0);
  while (node.ok()){
    tf::StampedTransform transform;
    /*
    try{
      //listener.lookupTransform("/turtle2", "/turtle1",
      //                         ros::Time(0), transform);
      listener.lookupTransform("/turtle2", "/carrot1",
                               ros::Time(0), transform);
    */
    try{
        ros::Time now = ros::Time::now(); //we can change this time to anything
        ros::Time delay_5 = ros::Time::now() - ros::Duration(5.0); //we can change this time to anything
        listener.waitForTransform(  "/turtle2", now,
                                    "/turtle1", delay_5,
                                    "/world",
                                    ros::Duration(3.0)); //(_,_,time,how long do I wait for it)
        listener.lookupTransform( "/turtle2", now,  //transform turtle2's now position
                                  "/turtle1", delay_5,  //to turtle1's position 5sec ago
                                  "/world", // name of the frame that is FIXED
                                  transform);
    }
    catch (tf::TransformException &ex) {
      ROS_ERROR("%s",ex.what());
      ros::Duration(1.0).sleep();
      continue;
    }

    geometry_msgs::Twist vel_msg;
    vel_msg.angular.z = 4.0 * atan2(transform.getOrigin().y(),
                                    transform.getOrigin().x());
    vel_msg.linear.x = 0.5 * sqrt(pow(transform.getOrigin().x(), 2) +
                                  pow(transform.getOrigin().y(), 2));
    turtle_vel.publish(vel_msg);

    rate.sleep();
  }
  return 0;
};