#include "ros/ros.h"
#include "visualization_msgs/MarkerArray.h"
#include <tf/transform_listener.h>
#include "visualization_msgs/Marker.h"

ros::Publisher g_marker_pub;
ros::Publisher g_array_pub;

void chatterCallback(const visualization_msgs::MarkerArray markers)
{
	//  ROS_INFO("I heard: [%d]",(int)markers.markers.size());
	ros::NodeHandle node;
	tf::TransformListener listener;
	tf::StampedTransform transform;
  	try{
		ros::Time now=ros::Time(0);
		listener.waitForTransform("/map","/camera_link",now, ros::Duration(5.0) );
 		listener.lookupTransform("/map", "/camera_link",now, transform);
  	}
  	catch (tf::TransformException ex){
  		ROS_ERROR("%s",ex.what());
	  	ros::Duration(1.0).sleep();
  	}

	visualization_msgs::MarkerArray mapMarkers;
	for(int i=0;i<markers.markers.size();i++){
		visualization_msgs::Marker marker;
		marker.header.frame_id="/map";
		marker.type=visualization_msgs::Marker::SPHERE;
		marker.action=visualization_msgs::Marker::ADD;
		marker.scale.x=0.2;
		marker.scale.y=0.2;
		marker.scale.z=0.2;
		marker.color.a=1.0;
		marker.pose.orientation.w=1.0;
		marker.pose.position.x=transform.getOrigin().x();
		marker.pose.position.y=transform.getOrigin().y();
		marker.pose.position.z=0.0;

		g_marker_pub.publish(marker);	

		mapMarkers.markers.push_back(marker);
	}

    
   	g_array_pub.publish(mapMarkers);	
	
	//ros::Publisher pub=node.advertise<visualization_msgs::MarkerArray>("/face_topic",1000);
	//pub.publish(mapMarkers);
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "face_node");

  ros::NodeHandle n;
  g_marker_pub = n.advertise<visualization_msgs::Marker> ("visualization_marker", 0);
  g_array_pub = n.advertise<visualization_msgs::MarkerArray> ("visualization_marker_array", 0);

  ros::Subscriber sub = n.subscribe("/facemapper/markers", 1000, chatterCallback);

  ros::spin();

  return 0;
}