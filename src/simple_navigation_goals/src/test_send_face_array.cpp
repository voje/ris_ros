#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

visualization_msgs::MarkerArray generate_marker_array(){
  visualization_msgs::MarkerArray ma;

  //posX, posY, orientZ, orientW
  double coords[][4] = {
    {-0.1299,  0.2711, -0.3035,  0.9528},
    {-0.9273,  0.8009,  0.9698,  0.2437}
  };

  for(int i=0; i<(sizeof(coords)/sizeof(*coords)); ++i){
    visualization_msgs::Marker marker;
    marker.header.frame_id = "map";
    marker.header.stamp = ros::Time::now();
    marker.ns = "face_marker";
    marker.id = i;

    marker.type = visualization_msgs::Marker::CUBE;
    marker.action = visualization_msgs::Marker::ADD;

    marker.pose.position.x = coords[i][0];
    marker.pose.position.y = coords[i][1];
    marker.pose.position.z = 0;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = coords[i][2];
    marker.pose.orientation.w = coords[i][3];

    // Set the scale of the marker -- 1x1x1 here means 1m on a side
    marker.scale.x = 0.2;
    marker.scale.y = 0.2;
    marker.scale.z = 0.2;

    // Set the color -- be sure to set alpha to something non-zero!
    marker.color.r = 1.0f;
    marker.color.g = 0.0f;
    marker.color.b = 0.0f;
    marker.color.a = 1.0;


    ma.markers.push_back(marker);
  }

  return ma;
}

int main(int argc, char** argv){
	ros::init(argc, argv, "test_send_face_array");	
	ros::NodeHandle n;
	ros::Publisher marr_pub = n.advertise<visualization_msgs::MarkerArray>("face_markers", 0);

	visualization_msgs::MarkerArray marr = generate_marker_array();

	ros::Rate loop_rate(10); //spin at frequency of 10hz

	while(ros::ok()){
		marr_pub.publish(marr);
		//ros::spinOnce();	//we would need this if we wanted callbacks
		loop_rate.sleep();	//the ros::Rate object will sleep for his set time
	}	

	ros::spin();

}