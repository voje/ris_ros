#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <geometry_msgs/PoseStamped.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;
typedef visualization_msgs::MarkerArray marr;
static int n_markers = 0;

void publish_marker(visualization_msgs::Marker, ros::NodeHandle);

visualization_msgs::MarkerArray generate_marker_array(){
  visualization_msgs::MarkerArray ma;

  //posX, posY, orientZ, orientW
  double coords[][4] = {
    {-0.1299,  0.2711, -0.3035,  0.9528},
    {-0.9273,  0.8009,  0.9698,  0.2437},
    {-0.9679,  2.0082,  0.1910,  0.9816},
    {-2.2727,  1.6802,  0.8720, -0.4895},
    {-2.7113,  2.9166,  0.5166,  0.8652},
    {-4.1678,  2.2879, -0.8449,  0.5349},
    {-4.1339,  3.3056,  0.6156,  0.7880},
    {-3.9150,  4.5890,  0.5584,  0.8296},
    {-3.4750,  5.5432,  0.5787,  0.8156},
    {-3.8438,  5.7909,  0.9784,  0.2067}
  };

  for(int i=0; i<(sizeof(coords)/sizeof(*coords)); ++i){
    ++n_markers;
    visualization_msgs::Marker marker;
    marker.header.frame_id = "map";
    marker.header.stamp = ros::Time::now();
    marker.ns = "nav_goal_marker";
    marker.id = i;

    marker.type = visualization_msgs::Marker::CUBE;
    marker.action = visualization_msgs::Marker::ADD;

    marker.pose.position.x = coords[i][0];
    marker.pose.position.y = coords[i][1];
    marker.pose.position.z = 0;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = coords[i][2];
    marker.pose.orientation.w = coords[i][3];

    // Set the scale of the marker -- 1x1x1 here means 1m on a side
    marker.scale.x = 0.2;
    marker.scale.y = 0.2;
    marker.scale.z = 0.2;

    // Set the color -- be sure to set alpha to something non-zero!
    marker.color.r = 0.0f;
    marker.color.g = 1.0f;
    marker.color.b = 0.0f;
    marker.color.a = 1.0;


    ma.markers.push_back(marker);
  }

  return ma;
}

actionlib::SimpleClientGoalState pursue_goal(visualization_msgs::Marker marker, ros::NodeHandle n){
  //publish our goal marker
  publish_marker(marker, n);

  //tell the action client that we want to spin a thread by default
  MoveBaseClient ac("move_base", true);

  //wait for the action server to come up
  while(!ac.waitForServer(ros::Duration(5.0))){
    ROS_INFO("Waiting for the move_base action server to come up");
  }

  move_base_msgs::MoveBaseGoal goal;

  //we'll send a goal to the robot to move 1 meter forward
  goal.target_pose.header.frame_id = "map";
  goal.target_pose.header.stamp = ros::Time::now();

  //-2,356 2,916 2,635
  goal.target_pose.pose.position.x = marker.pose.position.x;
  goal.target_pose.pose.position.y = marker.pose.position.y;
  goal.target_pose.pose.orientation.w = marker.pose.orientation.z;
  goal.target_pose.pose.orientation.w = marker.pose.orientation.w;

  ROS_INFO("Sending goal");
  ac.sendGoal(goal);

  ac.waitForResult();

  actionlib::SimpleClientGoalState state = ac.getState();
  if(state == actionlib::SimpleClientGoalState::SUCCEEDED)
    ROS_INFO("Success.");
  else
    ROS_INFO("Crash and burn!");
  return state;
}

void publish_marker(visualization_msgs::Marker marker, ros::NodeHandle n){
  ros::Publisher chatter_pub = n.advertise<visualization_msgs::Marker>("visualization_marker", 0); 
  chatter_pub.publish(marker); 
}

int main(int argc, char** argv){
  ros::init(argc, argv, "simple_navigation_goals");
  ros::NodeHandle n;
  //for local runs
  ros::Time::init();
  visualization_msgs::MarkerArray marr = generate_marker_array(); 
  ros::Publisher chatter_pub = n.advertise<visualization_msgs::Marker>("my_visualization_marker", 0); 
  while(ros::ok()){
    chatter_pub.publish(marr.markers[0]); 
  }
  /*
  for(int i=0; i<n_markers; ++i){
    pursue_goal(marr.markers[i], n);
  }
  */
}
