#include <iostream>
#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/Twist.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <nav_msgs/Odometry.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <math.h>
#include "simple_navigation_goals/visited_detected.h"
#include "simple_navigation_goals/reached.h"
#include "simple_navigation_goals/visiting_face.h"
#include "pos_transformer/vis_msg_mark.h"

#define INITIAL_GOALS_TOPIC "init_goal_markers"
#define FOUND_FACES_TOPIC "face_markers"

#define degrees(x) (180 * x / M_PI)

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;
typedef visualization_msgs::Marker Marker;
typedef visualization_msgs::MarkerArray MarkerArray;
int max_reached;

void publish_marker(Marker, ros::NodeHandle);

actionlib::SimpleClientGoalState pursue_goal(Marker marker, ros::NodeHandle n){
  //publish our goal marker
  publish_marker(marker, n);

  //tell the action client that we want to spin a thread by default
  MoveBaseClient ac("move_base", true);

  //wait for the action server to come up
  while(!ac.waitForServer(ros::Duration(5.0))){
    ROS_INFO("Waiting for the move_base action server to come up");
  }

  move_base_msgs::MoveBaseGoal goal;

  //we'll send a goal to the robot to move 1 meter forward
  goal.target_pose.header.frame_id = "map";
  goal.target_pose.header.stamp = ros::Time::now();

  //-2,356 2,916 2,635
  goal.target_pose.pose.position.x = marker.pose.position.x;
  goal.target_pose.pose.position.y = marker.pose.position.y;
  goal.target_pose.pose.orientation.w = marker.pose.orientation.z;
  goal.target_pose.pose.orientation.w = marker.pose.orientation.w;

  //ROS_INFO("Sending goal");
  ac.sendGoal(goal);

  ac.waitForResult();

  actionlib::SimpleClientGoalState state = ac.getState();
  if(state == actionlib::SimpleClientGoalState::SUCCEEDED){
    //ROS_INFO("Success.");
  }
  else{
    ROS_INFO("Crash and burn!");
  }
  return state;
}

void publish_marker(visualization_msgs::Marker marker, ros::NodeHandle n){
  ros::Publisher chatter_pub = n.advertise<visualization_msgs::Marker>("visualization_marker", 0); 
  chatter_pub.publish(marker); 
}

double get_yaw(ros::NodeHandle nh){
  //returns [0, 360] degrees
  boost::shared_ptr<nav_msgs::Odometry const> p_odom;
  p_odom = ros::topic::waitForMessage<nav_msgs::Odometry>("odom", nh, ros::Duration(0.5,0));
  //[-pi, pi]
  double ryaw = tf::getYaw(p_odom->pose.pose.orientation);
  double dyaw = degrees(ryaw);
  return dyaw + 180;
}

void rotate_360(ros::NodeHandle nh){
  ROS_INFO("rotating");
  ros::Publisher pub = nh.advertise<geometry_msgs::Twist>("cmd_vel_mux/input/navi", 1);
  geometry_msgs::Twist tw;
  tw.angular.z = 1.2; //in radians
  double init_yaw = get_yaw(nh);
  double new_yaw = init_yaw;
  double d_yaw = 0; //when it reaches 360, diff will be > 300
  while(d_yaw < 300){
    pub.publish(tw);
    d_yaw = new_yaw;
    new_yaw = get_yaw(nh);
    d_yaw = abs(d_yaw - new_yaw);
    //ROS_INFO("1yaw: %f", new_yaw); 
  }
  while(new_yaw < init_yaw){
    pub.publish(tw);
    new_yaw = get_yaw(nh);
    //ROS_INFO("2yaw: %f", new_yaw); 
  }
}

void confirm_reached(double x, double y, ros::NodeHandle nh){
  ros::ServiceClient client = nh.serviceClient<simple_navigation_goals::reached>("fs_reached");
  simple_navigation_goals::reached rch;
  rch.request.x = x;
  rch.request.y = y;
  client.call(rch);
}

//send map coordinates of faces you see, get back id of legit face
int legit_face_id(boost::shared_ptr<MarkerArray const> marr, ros::NodeHandle nh){
  ros::ServiceClient client = nh.serviceClient<simple_navigation_goals::visiting_face>("fs_visiting_face");
  simple_navigation_goals::visiting_face vis;
  ros::ServiceClient f_transform = nh.serviceClient<pos_transformer::vis_msg_mark>("cam_to_map");
  pos_transformer::vis_msg_mark transformed_mark;
  int boo = -1;
  int id = -1;
  for(int i=0; i<marr->markers.size(); i++){
    transformed_mark.request.in_marker = marr->markers[i];
    f_transform.call(transformed_mark);
    Marker m_map = transformed_mark.response.out_marker;
    vis.request.x = m_map.pose.position.x;
    vis.request.y = m_map.pose.position.y;

    if( client.call(vis) ){
      boo = vis.response.odgovor; 
    }
    if( boo != -1 ){
      id = marr->markers[i].id;
      break;
    }
  }
  return id;
}

Marker get_face_by_id(int id, boost::shared_ptr<MarkerArray const> marr){
  Marker m;
  for(int i=0; i<marr->markers.size(); i++){
    if(marr->markers[i].id == id){
     m = marr->markers[i];
     return m;
    }
  }
  return m;
}

int closest_face_index(MarkerArray marr){
  int indx = -1;
  double min_dist = 10000;
  for(int i=0; i<marr.markers.size(); i++){
    double dist = marr.markers[i].pose.position.z;
    if(dist < min_dist){
      min_dist = dist;
      indx = i;
    }
  }
  return indx;
}

//approach the nearest face
void approach_face(ros::NodeHandle nh){
  //ros::ServiceClient f_transform = nh.serviceClient<pos_transformer::vis_msg_mark>("cam_to_map");
  pos_transformer::vis_msg_mark transformed_mark; 
  Marker mm;
  int id = 0;
  ros::Publisher twist_pub = nh.advertise<geometry_msgs::Twist>("cmd_vel_mux/input/navi", 1);
  geometry_msgs::Twist tw;
  boost::shared_ptr<MarkerArray const> faces_in_sight;
  ROS_INFO("af_A");
  //rotate until you find a face
  faces_in_sight = ros::topic::waitForMessage<MarkerArray>("facemapper/markers", nh, ros::Duration(1,0));
  /*
  while( (id = legit_face_id(faces_in_sight, nh)) == -1 ){
    tw.angular.z = 1.5;  
    twist_pub.publish(tw);
    faces_in_sight = ros::topic::waitForMessage<MarkerArray>("facemapper/markers", nh, ros::Duration(1,0));
  }
  */
  bool a = true;
  while(faces_in_sight == NULL && a){
    tw.angular.z = 1.5;  
    twist_pub.publish(tw);
    faces_in_sight = ros::topic::waitForMessage<MarkerArray>("facemapper/markers", nh, ros::Duration(1,0));
    int iddd = closest_face_index(*faces_in_sight);
    if(faces_in_sight->markers[iddd].pose.position.z < 1.7){
      a = false;
    }
  }

  ROS_INFO("approaching face");
  int idx = closest_face_index(*faces_in_sight);
  ROS_INFO("closest_face_index: %d", idx);
  mm = faces_in_sight->markers[idx]; 
  double x_pos = mm.pose.position.x;
  double z_dist = mm.pose.position.z;
  //loop: move towards face and check conditions
  while( fabs(x_pos) >= 0.2 && z_dist <= 2 ){
    tw.angular.z = (x_pos/fabs(x_pos))*(0.1); //in radians
    twist_pub.publish(tw);

    faces_in_sight = ros::topic::waitForMessage<MarkerArray>("facemapper/markers", nh, ros::Duration(0.2,0));
    if( faces_in_sight == NULL ){
      continue;
    }
    idx = closest_face_index(*faces_in_sight);
    mm = faces_in_sight->markers[idx]; 
    double x_pos = mm.pose.position.x;
    double z_dist = mm.pose.position.z;
    std::cout << "asdf" << std::endl;
    ROS_INFO("x_pos: %f", x_pos);
  }
  while( fabs(x_pos) >= 0.02 || z_dist >= 0.5 ){
    tw.angular.z = (x_pos/fabs(x_pos))*(-0.1); //in radians
    //tw.angular.z = x_pos*(-0.2); //in radians
    tw.linear.x = (z_dist/fabs(z_dist))*(0.1);
    //tw.linear.x = z_dist*(0.2);
    twist_pub.publish(tw);

    faces_in_sight = ros::topic::waitForMessage<MarkerArray>("facemapper/markers", nh, ros::Duration(0.2,0));
    if( faces_in_sight == NULL ){
      ROS_INFO("ERR");
      return;
    }
    idx = closest_face_index(*faces_in_sight);
    mm = faces_in_sight->markers[idx]; 
    double x_pos = mm.pose.position.x;
    double z_dist = mm.pose.position.z;
    ROS_INFO("x_pos: %f, z_dist: %f", x_pos, z_dist);
    if( z_dist < 0.5 ){
      break;
    }
  }

  //todo transform the points to map
  //transformed_mark.request.in_marker = mm;
  //f_transform.call(transformed_mark);
  //double reached_map_x = transformed_mark.response.out_marker.pose.position.x;
  //double reached_map_y = transformed_mark.response.out_marker.pose.position.y;
  //confirm_reached(reached_map_x, reached_map_y, nh);
  ROS_INFO("face has been approached");
}

int main(int argc, char** argv){
  boost::shared_ptr<MarkerArray const> init_goals;
  boost::shared_ptr<simple_navigation_goals::visited_detected const> vi_det;
  ros::init(argc, argv, "simple_navigation_goals");
  ros::NodeHandle nh;
  //for local runs
  //ros::Time::init();

  //listen for init_marker_array
  init_goals = ( ros::topic::waitForMessage<MarkerArray>(INITIAL_GOALS_TOPIC, nh, ros::Duration(10,0)) );
  if(init_goals == NULL){
    ROS_INFO("No initial goals found.");
    return 1;
  }

  for(int i=0; i<init_goals->markers.size(); ++i){
    ROS_INFO("### pursuing %s %d", INITIAL_GOALS_TOPIC, i);
    //ros::Duration(2.0).sleep();
    if(pursue_goal(init_goals->markers[i], nh) == actionlib::SimpleClientGoalState::SUCCEEDED){
      ROS_INFO("%s %d reached", INITIAL_GOALS_TOPIC, i);
    }
    rotate_360(nh);
    rotate_360(nh);
    //listen to /visited_detected
    vi_det = ros::topic::waitForMessage<simple_navigation_goals::visited_detected>("visited_detected", nh, ros::Duration(2,0));
    if (vi_det->visited >= 2){
      return 0;
    }
    ROS_INFO("A");
    if( vi_det->visited < vi_det->detected ){
      approach_face(nh);
    }
    /*while( vi_det->visited < vi_det->detected ){
      ROS_INFO("B");
      vi_det = ros::topic::waitForMessage<simple_navigation_goals::visited_detected>("visited_detected", nh, ros::Duration(2,0));
      ROS_INFO("C");
      approach_face(nh);
      vi_det = ros::topic::waitForMessage<simple_navigation_goals::visited_detected>("visited_detected", nh, ros::Duration(2,0));
      ROS_INFO("D");
      if( vi_det->visited < vi_det->detected ){
        ROS_INFO("E");
        if(pursue_goal(init_goals->markers[i], nh) == actionlib::SimpleClientGoalState::SUCCEEDED){
          ROS_INFO("%s %d reached_again", INITIAL_GOALS_TOPIC, i);
        }
      }
    }//while
    */
  }//init_goals

}




  //useful: marr.markers.size()
  /*ros::Publisher chatter_pub = n.advertise<visualization_msgs::Marker>("my_visualization_marker", 0); 
  while(ros::ok()){
    chatter_pub.publish(marr.markers[0]); 
  }
  */
