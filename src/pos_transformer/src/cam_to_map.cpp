#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <tf/tf.h>
#include <geometry_msgs/PoseStamped.h>
#include <box_messages/TransformMe.h>

typedef box_messages::TransformMe Mark;
typedef geometry_msgs::PoseStamped Pose;

tf::TransformListener* listener;

bool callback(Mark::Request &req, Mark::Response &res){
	tf::StampedTransform transform;
	Pose in_pose;
	Pose out_pose;

	in_pose.header = req.in_marker.header;
	in_pose.pose = req.in_marker.pose;

	(*listener).waitForTransform("/map", "/base_link", ros::Time(0), ros::Duration(3.0));
	//listener.lookupTransform("/map", "/base_link", ros::Time(0), transform);
	(*listener).transformPose("/map", ros::Time(0), in_pose, "/base_link", out_pose);
	double t_x = transform.getOrigin().x();
	double t_y = transform.getOrigin().y();

	res.out_marker = req.in_marker;
	res.out_marker.header = out_pose.header;
	res.out_marker.pose = out_pose.pose;

	return true;
}

int main(int argc, char** argv){
	ros::init(argc, argv, "cam_to_map");
	ros::NodeHandle nh;	
	listener = new tf::TransformListener();

	ros::ServiceServer srv = nh.advertiseService("cam_to_map", callback);
	ROS_INFO("Server listening on cam_to_map");
	ros::spin();

	return 0;
}
